#define private public
#include "../../SobaZabave/src/Client/include/client.h"
#include <QDebug>
#include "catch.hpp"

TEST_CASE("Client.doConnect", "[function]")
{
    SECTION("Client function doConnect returns false if client did not connect to server since server is not running")
    {
        // Arrange
        bool expectedReturnValue = false;
        // Act
        Client client = Client();
        bool output = client.doConnect();
        // Assert
        REQUIRE(output == expectedReturnValue);
    }

}


TEST_CASE("Client.constructor", "[constructor]")
{
    SECTION("Client constructor does not throw exception when called")
    {
        // Arrange
        Client c;
        // Act
        // Assert
        REQUIRE_NOTHROW(c);
    }

}

TEST_CASE("Client.deconstructor", "[deconstructor]")
{
    SECTION("Client destructor does not throw exception when called")
    {
        // Arrange
        Client* client = new Client();
        delete client;

        // Act
        // Assert
        REQUIRE_NOTHROW(client);
    }

}
