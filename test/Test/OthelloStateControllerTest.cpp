#define private public

#include "../../SobaZabave/src/Client/include/othellostatecontroller.h"
#include "../../SobaZabave/src/Client/include/othellostate.h"



TEST_CASE("OthellStateController", "[namespace][function]")
{

    SECTION("PlayNoMove only changes OnMove variable in othellostate object")
    {
        // Arrange
        Othellostate o;
        int expectedOutput = o.getOnMove() * -1;
        // Act
        OthelloStateController::playNoMove(o);
        int output = o.getOnMove();
        // Assert
        CHECK( (expectedOutput == -1 || expectedOutput == 1) );
        CHECK( (output == -1 || output == 1) );
        REQUIRE(expectedOutput == output);
    }

    SECTION("PlayMove chenges OnMove variable, puts players id to table field "
            "table[move/10][move%10] and flips table if you can ")
    {
        SECTION("move = 10 field to be changed is table[1][0]"){

            // Arrange
            Othellostate o;
            int expectedOnMoveOutput = o.getOnMove() * -1;

            int startTable[8][8];
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    startTable[i][j] = o.table[i][j];
            int move = 10;
            int row = move/10;
            int col = move%10;
            bool expectedIndOutput = true;
            bool indOutput = true;
            // Act
            OthelloStateController::playMove(o, move);
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(row == i && col == j){
                        if(o.table[i][j] != expectedOnMoveOutput * (-1)){
                            indOutput = false;
                            continue;
                        }
                    }
                    else if(startTable[i][j] != o.table[i][j]){
                        indOutput = false;
                    }
                }
            }
            int onMoveOutput = o.getOnMove();
            // Assert
            REQUIRE( expectedOnMoveOutput == onMoveOutput );
            REQUIRE( expectedIndOutput == indOutput);
        }

        SECTION("move = 0 field to be changed is table[0][0]"){

            // Arrange
            Othellostate o;
            int expectedOnMoveOutput = o.getOnMove() * -1;

            int startTable[8][8];
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    startTable[i][j] = o.table[i][j];
            int move = 0;
            int row = move/10;
            int col = move%10;
            bool expectedIndOutput = true;
            bool indOutput = true;
            // Act
            OthelloStateController::playMove(o, move);
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(row == i && col == j){
                        if(o.table[i][j] != expectedOnMoveOutput * (-1)){
                            indOutput = false;
                            continue;
                        }
                    }
                    else if(startTable[i][j] != o.table[i][j]){
                        indOutput = false;
                    }
                }
            }
            int onMoveOutput = o.getOnMove();
            // Assert
            REQUIRE( expectedOnMoveOutput == onMoveOutput );
            REQUIRE( expectedIndOutput == indOutput);
        }

        SECTION("move = 35 field to be changed is table[3][5], table[3][4] is flipped too"){

            // Arrange
            Othellostate o;
            int expectedOnMoveOutput = o.getOnMove() * -1;

            int startTable[8][8];
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    startTable[i][j] = o.table[i][j];
            int move = 35;

            bool expectedIndOutput = true;
            bool indOutput = true;
            // Act
            OthelloStateController::playMove(o, move);
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(i == 3 && j == 5 || i == 3  && j == 4){
                        if(o.table[i][j] != expectedOnMoveOutput * (-1)){
                            indOutput = false;
                            continue;
                        }
                    }
                    else if(startTable[i][j] != o.table[i][j]){
                        indOutput = false;
                    }
                }
            }
            int onMoveOutput = o.getOnMove();
            // Assert
            REQUIRE( expectedOnMoveOutput == onMoveOutput );
            REQUIRE( expectedIndOutput == indOutput);
        }

        SECTION("move = 53, field to be changed is table[5][3], table[4][3] is flipped too"){

            // Arrange
            Othellostate o;
            int expectedOnMoveOutput = o.getOnMove() * -1;

            int startTable[8][8];
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    startTable[i][j] = o.table[i][j];
            int move = 53;

            bool expectedIndOutput = true;
            bool indOutput = true;
            // Act
            OthelloStateController::playMove(o, move);
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(i == 5 && j == 3 || i == 4 && j == 3){
                        if(o.table[i][j] != expectedOnMoveOutput * (-1)){
                            indOutput = false;
                            continue;
                        }
                    }
                    else if(startTable[i][j] != o.table[i][j]){
                        indOutput = false;
                    }
                }
            }
            int onMoveOutput = o.getOnMove();
            // Assert
            REQUIRE( expectedOnMoveOutput == onMoveOutput );
            REQUIRE( expectedIndOutput == indOutput);
        }

        SECTION("after playMove(o,35) and second playMove(o,23)"
                "  table[2][3], table[3][3], table[4,3] should be player2"
                " table[3][4], table[3][5], table[4,4] should be player1"
                " onMove should be player1"){

            // Arrange
            Othellostate o;
            int expectedOnMoveOutput = o.getOnMove();

            int startTable[8][8];
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    startTable[i][j] = o.table[i][j];
            int move1 = 35;
            int move2 = 23;
            bool expectedIndOutput = true;
            bool indOutput = true;
            // Act

            OthelloStateController::playMove(o, move1);
            OthelloStateController::playMove(o, move2);

            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(i == 3 && j == 5 || i == 3  && j == 4 || i == 4 && j == 4){
                        if(o.table[i][j] != expectedOnMoveOutput ){
                            indOutput = false;
                        }
                    }else if(i == 2 && j == 3 || i == 3  && j == 3 || i == 4 && j == 3){
                        if(o.table[i][j] != expectedOnMoveOutput *(-1) ){
                            indOutput = false;
                        }
                    }
                    else if(startTable[i][j] != o.table[i][j]){
                        indOutput = false;
                    }
                }
            }
            int onMoveOutput = o.getOnMove();
            // Assert
            REQUIRE( expectedOnMoveOutput == onMoveOutput );
            REQUIRE( expectedIndOutput == indOutput);
        }
        SECTION("after playMove(o,24) and second playMove(o,23)"
                "  table[2][3], table[3][3], table[4,3] should be player2"
                " table[2][4], table[3][4], table[4,4] should be player1"
                " onMove should be player1"){

            // Arrange
            Othellostate o;
            int expectedOnMoveOutput = o.getOnMove();

            int startTable[8][8];
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    startTable[i][j] = o.table[i][j];
            int move1 = 24;
            int move2 = 23;
            bool expectedIndOutput = true;
            bool indOutput = true;
            // Act

            OthelloStateController::playMove(o, move1);
            OthelloStateController::playMove(o, move2);

            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(i == 2 && j == 4 || i == 3  && j == 4 || i == 4 && j == 4){
                        if(o.table[i][j] != expectedOnMoveOutput ){
                            indOutput = false;
                        }
                    }else if(i == 2 && j == 3 || i == 3  && j == 3 || i == 4 && j == 3){
                        if(o.table[i][j] != expectedOnMoveOutput *(-1) ){
                            indOutput = false;
                        }
                    }
                    else if(startTable[i][j] != o.table[i][j]){
                        indOutput = false;
                    }
                }
            }
            int onMoveOutput = o.getOnMove();
            // Assert
            REQUIRE( expectedOnMoveOutput == onMoveOutput );
            REQUIRE( expectedIndOutput == indOutput);
        }

    }

    SECTION("FlipTable flips all elements between field where you put token"
            "and every field with the same id if you can draw line between them"
            "and every field in between is opposite id")
    {
        SECTION("With detault starting state on fliptable(o, 3, 5) "
                "table field between [3][5] and [3][3] should be flipped"){

            // Arrange
            Othellostate o;
            int startTable[8][8];
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    startTable[i][j] = o.table[i][j];
            int row = 3;
            int col = 5;
            bool expectedIndOutput = true;
            bool indOutput = true;
            // Act
            OthelloStateController::playMove(o,row*10+col);
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(i == 3  && j == 4 || i ==3 && j == 5){
                        if(o.table[i][j] != o.table[3][5]){
                            indOutput = false;
                            continue;
                        }
                    }
                    else if(startTable[i][j] != o.table[i][j]){
                        indOutput = false;
                    }
                }
            }
            // Assert
            REQUIRE( expectedIndOutput == indOutput);
        }
        SECTION("With detault starting state on fliptable(o, 2, 4) "
                "table field between [2][4] and [4][4] should be flipped"){

            // Arrange
            Othellostate o;
            int startTable[8][8];
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    startTable[i][j] = o.table[i][j];
            int row = 2;
            int col = 4;
            bool expectedIndOutput = true;
            bool indOutput = true;
            // Act
            OthelloStateController::playMove(o,row*10+col);
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(i == 2  && j == 4 || i == 3 && j == 4){
                        if(o.table[i][j] != o.table[2][4]){
                            indOutput = false;
                            continue;
                        }
                    }
                    else if(startTable[i][j] != o.table[i][j]){
                        indOutput = false;
                    }
                }
            }
            // Assert
            REQUIRE( expectedIndOutput == indOutput);
        }
        SECTION("With detault starting state on fliptable(o, 5, 3) "
                "table field between [5][3] and [3][3] should be flipped"){

            // Arrange
            Othellostate o;
            int startTable[8][8];
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    startTable[i][j] = o.table[i][j];
            int row = 5;
            int col = 3;
            bool expectedIndOutput = true;
            bool indOutput = true;
            // Act
            OthelloStateController::playMove(o,row*10+col);
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(i == 5  && j == 3 || i == 4 && j == 3){
                        if(o.table[i][j] != o.table[5][3]){
                            indOutput = false;
                            continue;
                        }
                    }
                    else if(startTable[i][j] != o.table[i][j]){
                        indOutput = false;
                    }
                }
            }
            // Assert
            REQUIRE( expectedIndOutput == indOutput);
        }


    }

    SECTION("HasNextMove should return true if player can put token anywhere on table"
            "else it returns false")
    {
        SECTION("At default start if should return true"){

            // Arrange
            Othellostate o;
            bool expectedOutput = true;
            // Act
            int output = OthelloStateController::hasNextMove(o);
            // Assert
            REQUIRE( expectedOutput == output);
        }

        SECTION("At default start and played move playMove(o,35) if should return true"){

            // Arrange
            Othellostate o;
            bool expectedOutput = true;
            // Act
            OthelloStateController::playMove(o,35);
            int output = OthelloStateController::hasNextMove(o);
            // Assert
            REQUIRE( expectedOutput == output);
        }
        SECTION("At default start and played move playMove(o,53) if should return true"){

            // Arrange
            Othellostate o;
            bool expectedOutput = true;
            // Act
            OthelloStateController::playMove(o,53);
            int output = OthelloStateController::hasNextMove(o);
            // Assert
            REQUIRE( expectedOutput == output);
        }
        SECTION("At default start and played moves playMove(o,35),"
                "playMove(o,25) if should return true"){

            // Arrange
            Othellostate o;
            bool expectedOutput = true;
            // Act
            OthelloStateController::playMove(o,35);
            OthelloStateController::playMove(o,25);
            int output = OthelloStateController::hasNextMove(o);
            // Assert
            REQUIRE( expectedOutput == output);
        }

        SECTION("At default start and played moves playMove(o,35),"
                "playMove(o,25), playMove(o,24), playMove(o,23) if should return true"){

            // Arrange
            Othellostate o;
            bool expectedOutput = true;
            // Act
            OthelloStateController::playMove(o,35);
            OthelloStateController::playMove(o,25);
            OthelloStateController::playMove(o,24);
            OthelloStateController::playMove(o,23);
            int output = OthelloStateController::hasNextMove(o);
            // Assert
            REQUIRE( expectedOutput == output);
        }


    }

    SECTION("GetNextValidMoves gives you all fields where you can put your token")
    {
        SECTION("At default start if should return Qvector with 24,42,35,53"){

            // Arrange
            Othellostate o;
            bool expectedIndOutput = true;
            bool indOutput = true;
            QVector<int> expectedOutputVector{24,35,42,53};
            // Act
            QVector<int> output = OthelloStateController::getNextValidMoves(o);
            for(int i = 0; i < output.size(); ++i){
                if(output[i] != expectedOutputVector[i]){
                    indOutput = false;
                    break;
                }
            }
            // Assert
            CHECK( output.size()== expectedOutputVector.size());
            REQUIRE( expectedIndOutput == indOutput);
        }

        SECTION("At default start and played move 35,"
                " it should return Qvector with 23,25,45"){

            // Arrange
            Othellostate o;
            bool expectedIndOutput = true;
            bool indOutput = true;
            QVector<int> expectedOutputVector{23,25,45};
            // Act
            OthelloStateController::playMove(o,35);
            QVector<int> output = OthelloStateController::getNextValidMoves(o);
            for(int i = 0; i < output.size(); ++i){
                if(output[i] != expectedOutputVector[i]){
                    indOutput = false;
                    break;
                }
            }
            // Assert
            CHECK( output.size()== expectedOutputVector.size());
            REQUIRE( expectedIndOutput == indOutput);
        }

        SECTION("At default start and played moves 35 and 45"
                " it should return Qvector with 52,53,54,55,56"){

            // Arrange
            Othellostate o;
            bool expectedIndOutput = true;
            bool indOutput = true;
            QVector<int> expectedOutputVector{52,53,54,55,56};
            // Act
            OthelloStateController::playMove(o,35);
            OthelloStateController::playMove(o,45);
            QVector<int> output = OthelloStateController::getNextValidMoves(o);
            for(int i = 0; i < output.size(); ++i){
                if(output[i] != expectedOutputVector[i]){
                    indOutput = false;
                    break;
                }
            }
            // Assert
            CHECK( output.size()== expectedOutputVector.size());
            REQUIRE( expectedIndOutput == indOutput);
        }
    }

    SECTION("isPossibleMove should return true if player who is on move "
            "can put his token on field table[row][col]")
    {
        SECTION("At default start isPossibleMove(o,3,5) should return true"){

            // Arrange
            Othellostate o;
            bool expectedIndOutput = true;
            // Act
            bool indOutput = OthelloStateController::isPossibleMove(o,3,5);
            // Assert
            REQUIRE( expectedIndOutput == indOutput);
        }

        SECTION("At default start isPossibleMove(o,3,4) should return false"){

            // Arrange
            Othellostate o;
            bool expectedIndOutput = false;
            // Act
            bool indOutput = OthelloStateController::isPossibleMove(o,3,4);
            // Assert
            REQUIRE( expectedIndOutput == indOutput);
        }

        SECTION("At default start isPossibleMove(o,1,0) should return false"){

            // Arrange
            Othellostate o;
            bool expectedIndOutput = false;
            // Act
            bool indOutput = OthelloStateController::isPossibleMove(o,1,0);
            // Assert
            REQUIRE( expectedIndOutput == indOutput);
        }
        SECTION("At default start and played move 35"
                "isPossibleMove(o,2,5) should return true"){

            // Arrange
            Othellostate o;
            bool expectedIndOutput = true;
            // Act
            OthelloStateController::playMove(o,35);
            bool indOutput = OthelloStateController::isPossibleMove(o,2,5);
            // Assert
            REQUIRE( expectedIndOutput == indOutput);
        }

        SECTION("At default start and played moves 35 and 25"
                "isPossibleMove(o,5,3) should return true"){

            // Arrange
            Othellostate o;
            bool expectedIndOutput = true;
            // Act
            OthelloStateController::playMove(o,35);
            OthelloStateController::playMove(o,25);
            bool indOutput = OthelloStateController::isPossibleMove(o,5,3);
            // Assert
            REQUIRE( expectedIndOutput == indOutput);
        }

    }

}



