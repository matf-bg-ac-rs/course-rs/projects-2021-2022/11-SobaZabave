QT = core \
    network \
    gui \
    widgets

TEMPLATE = app

CONFIG += c++17 \
        console

HEADERS += ../../SobaZabave/src/Client/include/othellostatecontroller.h \
    ../../SobaZabave/src/Client/include/othellogame.h \
    ../../SobaZabave/src/Client/include/othellominmax.h \
    ../../SobaZabave/src/Client/include/othellostate.h \
    ../../SobaZabave/src/Client/include/client.h \
    ../../SobaZabave/src/Client/include/gamesettings.h \
    ../../SobaZabave/src/Client/include/othelloplayer.h \
    ../../SobaZabave/src/Client/include/player.h \
    ../../SobaZabave/src/Client/include/miceboard.h \
    ../../SobaZabave/src/Client/include/micegame.h \
    ../../SobaZabave/src/Client/include/miceplayer.h



SOURCES += \
    OthelloGameTest.cpp \
    ClientTest.cpp \
    OthelloMinMaxTest.cpp \
    OthelloStateControllerTest.cpp \
    OthelloStateTest.cpp \
    MiceGameTest.cpp \
    MiceBoardTest.cpp \
    test.cpp \
    ../../SobaZabave/src/Client/src/othellostatecontroller.cpp \
    ../../SobaZabave/src/Client/src/othellogame.cpp \
    ../../SobaZabave/src/Client/src/othellostate.cpp \
    ../../SobaZabave/src/Client/src/client.cpp \
    ../../SobaZabave/src/Client/src/othellominmax.cpp \
    ../../SobaZabave/src/Client/src/gamesettings.cpp \
    ../../SobaZabave/src/Client/src/othelloplayer.cpp \
    ../../SobaZabave/src/Client/src/player.cpp \
    ../../SobaZabave/src/Client/src/miceboard.cpp \
    ../../SobaZabave/src/Client/src/micegame.cpp \
    ../../SobaZabave/src/Client/src/miceplayer.cpp






TARGET = Test

DISTFILES +=
