#define private public
#include "../../SobaZabave/src/Client/include/othellominmax.h"
#include <QDebug>
#include "catch.hpp"

TEST_CASE("OthelloMinMax.evaluate", "[function]")
{
    SECTION("Function Evaluate evaluate state of table with INT_MAX if player is a winner")
    {
        // Arrange
        GameSettings gs;
        OthelloMinMax othello = OthelloMinMax(gs);
        Othellostate os;
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                os.table[i][j] = 1;
        int move = os.onMove;
        int expected = move * INT_MAX;
        // Act
        int output = othello.Evaluate(os);
        // Assert
        REQUIRE(output == expected);
    }
    SECTION("Function Evaluate evaluate state of table with HeuristicScore if game is not finished")
    {
        // Arrange
        GameSettings gs;
        OthelloMinMax othello = OthelloMinMax(gs);
        Othellostate os;
        os.table[1][1] = 0;
        int expected = othello.HeuristicScore(os);
        // Act
        int output = othello.Evaluate(os);
        // Assert
        REQUIRE(output == expected);
    }



}
TEST_CASE("OthelloMinMax.HeuristicScore", "[function]")
{
    SECTION("Function HeuristicScore return zero when on table is only zeros")
    {
        // Arrange
        GameSettings gs;
        OthelloMinMax othello = OthelloMinMax(gs);
        Othellostate os;
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                os.table[i][j] = 0;
        int expecterOutput = 0;
        // Act
        int output = othello.HeuristicScore(os);
        // Assert
        REQUIRE(output == expecterOutput);
    }
    SECTION("Function HeuristicScore returns a positive value if there is more 1 value than -1 value on the table")
    {
        // Arrange
        GameSettings gs;
        OthelloMinMax othello = OthelloMinMax(gs);
        Othellostate os;
        for(int i = 0; i < 6; i++)
            for(int j = 0; j < 6; j++)
                os.table[i][j] = 1;
        bool expectedOutput = true;
        // Act
        int heuristicScore = othello.HeuristicScore(os);
        bool output = heuristicScore > 0;

        // Assert
        REQUIRE(output == expectedOutput);
    }
    SECTION("Function HeuristicScore returns a negative value if there is more -1 value than 1 value on the table")
    {
        // Arrange
        GameSettings gs;
        OthelloMinMax othello = OthelloMinMax(gs);
        Othellostate os;
        for(int i = 0; i < 6; i++)
            for(int j = 0; j < 6; j++)
                os.table[i][j] = -1;
        bool expectedOutput = true;
        // Act
        int heuristicScore = othello.HeuristicScore(os);
        bool output = heuristicScore < 0;

        // Assert
        REQUIRE(output == expectedOutput);
    }
    SECTION("Function HeuristicScore returns negative value if there is equal number of -1 and 1 value on the table")
    {
        // Arrange
        GameSettings gs;
        OthelloMinMax othello = OthelloMinMax(gs);
        Othellostate os;
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                os.table[i][j] = 1;
        for(int i = 4; i < 8; i++)
            for(int j = 4; j < 8; j++)
                os.table[i][j] = -1;
        int expectedOutput = 0;
        // Act
        int output = othello.HeuristicScore(os);


        // Assert
        REQUIRE(output < expectedOutput);
    }


}

