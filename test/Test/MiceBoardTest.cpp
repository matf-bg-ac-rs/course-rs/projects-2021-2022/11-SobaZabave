#define private public

#include "../../SobaZabave/src/Client/include/miceboard.h"
#include "catch.hpp"

TEST_CASE("MiceBoard", "[class]")
{
        SECTION("setTable puts on field player's id")
        {
            // Arrange
            MiceBoard miceBoard;
            int playerId = 1;
            int fieldId = 22;
            int expectedResult = 1;

            // Act
            miceBoard.setTable(fieldId, playerId);

            // Assert
            REQUIRE(miceBoard.m_table[fieldId] == expectedResult);
        }

        SECTION("checkIsValidMovePhase0and2 at the begging of the game returns always true"
                "because all fields are empty")
        {
            //Arrange
            MiceBoard miceBoard;
            int move = 21;
            bool expectedResult = true;

            //Act
            bool result = miceBoard.checkIsValidMovePhase0and2(move);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase0and2 return false if id of field on which we"
                "try to set figure is bigger than table size")
        {
            //Arrange
            MiceBoard miceBoard;
            int move = 55;
            bool expectedResult = false;

            //Act
            bool result = miceBoard.checkIsValidMovePhase0and2(move);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase0and2 return false if field on which we"
                "try to set figure is lower than 0")
        {
            //Arrange
            MiceBoard miceBoard;
            int move = -40;
            bool expectedResult = false;

            //Act
            bool result = miceBoard.checkIsValidMovePhase0and2(move);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase0and2 return true if the field we want to move is empty")
        {
            //Arrange
            MiceBoard miceBoard;
            for(int i = 0; i < 16; i++)
            {
                miceBoard.m_table[i] = 1;
                miceBoard.m_emptyPositions.erase(i);
            }
            int move = 16;
            bool expectedResult = true;

            //Act
            bool result = miceBoard.checkIsValidMovePhase0and2(move);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase0and2 return false if the field we want to move is not empty")
        {
            //Arrange
            MiceBoard miceBoard;
            for(int i = 0; i < 16; i++)
            {
                miceBoard.m_table[i] = 1;
                miceBoard.m_emptyPositions.erase(i);
            }

            int move = 14;
            bool expectedResult = false;

            //Act
            bool result = miceBoard.checkIsValidMovePhase0and2(move);

            //Assert
            REQUIRE(result == expectedResult);


        }

        SECTION("checkIsValidMovePhase0and2 return false if doesn't exists empty field")
        {
            //Arrange
            MiceBoard miceBoard;
            for(int i = 0; i < miceBoard.numberOfFields; i++)
            {
                miceBoard.m_table[i] = 1;
                miceBoard.m_emptyPositions.erase(i);
            }
            int move = 20;
            bool expectedResult = false;

            //Act
            bool result = miceBoard.checkIsValidMovePhase0and2(move);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase1 return false if player's id is not valid (this player doesn't play this game)")
        {
            //Arrange
            MiceBoard miceBoard;
            QPair<int, int> move = qMakePair(1,2);
            int player = 22;
            bool expectedResult = false;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Arrange
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase1 return false if player's move has source or destination bigger than table size")
        {
            //Arrange
            MiceBoard miceBoard;
            QPair<int, int> move = qMakePair(25,1);
            int player = 0;
            bool expectedResult = false;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Arrange
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase1 return false if player's move has source or destination lower than 0")
        {
            //Arrange
            MiceBoard miceBoard;
            QPair<int, int> move = qMakePair(3,-100);
            int player = 0;
            bool expectedResult = false;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Arrange
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase1 return true if the field we want to move is empty"
                "and source and destination are connected in graph in the first circle")
        {
            //Arrange
            MiceBoard miceBoard;
            for(int i = 0; i < 8; i += 2)
            {
                miceBoard.m_table[i] = 1;
                miceBoard.m_emptyPositions.erase(i);
            }
            QPair<int, int> move = qMakePair(0, 1);
            int player = 1;
            bool expectedResult = true;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase1 return true if the field we want to move is empty"
                "and source and destination are connected in graph in the second circle")
        {
            //Arrange
            MiceBoard miceBoard;
            for(int i = 8; i < 16; i += 2)
            {
                miceBoard.m_table[i] = 1;
                miceBoard.m_emptyPositions.erase(i);
            }
            QPair<int, int> move = qMakePair(12, 13);
            int player = 1;
            bool expectedResult = true;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase1 return true if the field we want to move is empty"
                "and source and destination are connected in graph in the third circle")
        {
            //Arrange
            MiceBoard miceBoard;
            for(int i = 16; i < 24; i += 2)
            {
                miceBoard.m_table[i] = 1;
                miceBoard.m_emptyPositions.erase(i);
            }
            QPair<int, int> move = qMakePair(22, 23);
            int player = 1;
            bool expectedResult = true;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase1 return true if the field we want to move is empty"
                "and source and destination are connected in graph, connection first and second circle")
        {
            //Arrange
            MiceBoard miceBoard;
            miceBoard.m_table[1] = 1;
            QPair<int, int> move = qMakePair(1, 9);
            int player = 1;
            bool expectedResult = true;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase1 return true if the field we want to move is empty"
                "and source and destination are connected in graph, connection second and third circle")
        {
            //Arrange
            MiceBoard miceBoard;
            miceBoard.m_table[15] = 1;
            QPair<int, int> move = qMakePair(15, 23);
            int player = 1;
            bool expectedResult = true;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase1 return false if the source and destination are not connected in graph")
        {
            //Arrange
            MiceBoard miceBoard;
            miceBoard.m_table[0] = 1;
            miceBoard.m_table[23] = 0;
            QPair<int, int> move = qMakePair(0,23);
            int player = 1;
            bool expectedResult = false;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Assert
            REQUIRE(result == expectedResult);

        }

        SECTION("checkIsValidMovePhase1 return false if the destination is not empty field")
        {
            //Arrange
            MiceBoard miceBoard;
            miceBoard.m_table[18] = 1;
            QPair<int, int> move = qMakePair(19, 18);
            int player = 1;
            bool expectedResult = false;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("checkIsValidMovePhase1 return false if the source is not player's field")
        {
            //Arrange
            MiceBoard miceBoard;
            miceBoard.m_table[19] = 2;
            QPair<int, int> move = qMakePair(19, 18);
            int player = 1;
            bool expectedResult = false;

            //Act
            bool result = miceBoard.checkIsValidMovePhase1(move, player);

            //Assert
            REQUIRE(result == expectedResult);
        }

        SECTION("Constructor exists")
        {
            // Arrange
            MiceBoard miceBoard;

            // Assert
            REQUIRE_NOTHROW(miceBoard);
        }

        SECTION("Destructor exists")
        {
            // Arrange
            MiceBoard* miceBoard = new MiceBoard();
            delete miceBoard;

            // Assert
            REQUIRE_NOTHROW(miceBoard);
        }


}
