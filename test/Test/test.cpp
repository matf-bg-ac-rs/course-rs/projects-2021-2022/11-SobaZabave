#define CATCH_CONFIG_MAIN

#include "catch.hpp"


#include "OthelloStateControllerTest.cpp"
#include "OthelloGameTest.cpp"
#include "MiceGameTest.cpp"
#include "OthelloStateTest.cpp"
#include "ClientTest.cpp"
#include "OthelloMinMaxTest.cpp"
#include "MiceBoardTest.cpp"
