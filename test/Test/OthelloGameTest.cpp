#define private public
#include "../../SobaZabave/src/Client/include/othellogame.h"
#include "catch.hpp"


TEST_CASE("OthelloGame constructor", "[constructor]")
{

    SECTION("Othello Game constructor succesfully set value of typeOfGame as Local")
    {
        // Arrange
        GameSettings gameSettings = GameSettings();
        GameSettings::GameType expectedTypeOfGame = GameSettings::GameType::LOCAL;

        // Act
        OthelloGame othelloGame = OthelloGame(gameSettings);
        GameSettings::GameType outputGameType = othelloGame.typeOfGame;
        // Assert
        REQUIRE(outputGameType == expectedTypeOfGame);

    }


    SECTION("Othello Game constructor succesfully set atribut gameSettings on gs which is parameter of constructor")
    {
        // Arrange
        GameSettings gs = GameSettings();
        GameSettings::GameDifficulty expectedGameDiff = GameSettings::GameDifficulty::MEDIUM;
        gs.setDifficultyLevel(expectedGameDiff);

        // Act
        OthelloGame othelloGame = OthelloGame(gs);
        GameSettings::GameDifficulty output = othelloGame.gameSettings.getdifficultyLevel();
        // Assert
        REQUIRE(output == expectedGameDiff);
    }
    SECTION("Othello Game constructor does not throw exception when called")
    {
        // Arrange
        GameSettings gameSettings = GameSettings();

        // Act
        OthelloGame othelloGame = OthelloGame(gameSettings);

        // Assert
        REQUIRE_NOTHROW(othelloGame);
    }
}

TEST_CASE("doMove", "[function]")
{

    SECTION("Function doMove(move) doesn't change the table if parameter move is -1 (invalid move)")
    {
        //Arrange
        GameSettings gameSettings = GameSettings();
        OthelloGame othelloGame = OthelloGame(gameSettings);
        int startTable[8][8];
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                startTable[i][j] = othelloGame.currentState.table[i][j];
        int move = -1;
        bool expectedOutput = true;

        // Act
        othelloGame.doMove(move);
        bool outputIsSame = true;
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                if(othelloGame.currentState.table[i][j] != startTable[i][j])
                    outputIsSame = false;


        // Assert
        REQUIRE(outputIsSame == expectedOutput);
    }
    SECTION("Function doMove(move) change the table field on position [0][0] if parameter move is zero (valid move) ")
    {
        // Arrange
        GameSettings gameSettings = GameSettings();
        OthelloGame othelloGame = OthelloGame(gameSettings);
        int move = 0;
        int expectedValue = othelloGame.currentState.table[0][0]; //0

        // Act
        othelloGame.doMove(move);
        int outputValue = othelloGame.currentState.table[0][0];

        // Assert
        REQUIRE_FALSE(expectedValue == outputValue);
    }
    SECTION("Function doMove(move) change the table field if parameter move is 22 (valid move) ")
    {
        // Arrange
        GameSettings gameSettings = GameSettings();
        OthelloGame othelloGame = OthelloGame(gameSettings);
        int move = 22;
        int expectedValue = othelloGame.currentState.table[2][2]; //0

        // Act
        othelloGame.doMove(move);
        int outputValue = othelloGame.currentState.table[2][2];

        // Assert
        REQUIRE_FALSE(expectedValue == outputValue);
    }

}

TEST_CASE("getWinner", "[function]")
{

    SECTION("If all values on table are ones, function returns that player1 is winner ")
    {
        // Arrange
        GameSettings gameSettings = GameSettings();
        OthelloGame othelloGame = OthelloGame(gameSettings);
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                othelloGame.currentState.table[i][j] = 1;
        int expectedPlayer = 1;
        // Act
        int outputPlayer = othelloGame.getWinner();
        // Assert
        REQUIRE(outputPlayer == expectedPlayer);
    }
    SECTION("If all values on table are minus ones, function returns that player2 is winner ")
    {
        // Arrange
        GameSettings gameSettings = GameSettings();
        OthelloGame othelloGame = OthelloGame(gameSettings);
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                othelloGame.currentState.table[i][j] = -1;
        int expectedPlayer = -1;
        // Act
        int outputPlayer = othelloGame.getWinner();
        // Assert
        REQUIRE(outputPlayer == expectedPlayer);
    }
    SECTION("If half of values on table are -1 and other half are +1, function returns that player2 is winner")
    {
        // Arrange
        GameSettings gameSettings = GameSettings();
        OthelloGame othelloGame = OthelloGame(gameSettings);
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                othelloGame.currentState.table[i][j] = -1;
        for(int i = 4; i < 8; i++)
            for(int j = 4; j < 8; j++)
                othelloGame.currentState.table[i][j] = 1;
        int expectedPlayer = -1;
        // Act
        int outputPlayer = othelloGame.getWinner();
        // Assert
        REQUIRE(outputPlayer == expectedPlayer);
    }

}

TEST_CASE("getNextMove", "[function]")
{

    SECTION("Function getNextMove returns move that belongs to field that previously contained zero")
    {
        // Arrange
        GameSettings gameSettings = GameSettings();
        OthelloGame othelloGame = OthelloGame(gameSettings);
        int expectedOutput = 0;

        // Act
        int move = othelloGame.get_next_move();
        int output = othelloGame.currentState.table[move/10][move%10];

        // Assert
        REQUIRE(output == expectedOutput);
    }
    SECTION("Function getNextMove return -1 if table  have only -1 field value")
    {
        // Arrange
        GameSettings gameSettings = GameSettings();
        OthelloGame othelloGame = OthelloGame(gameSettings);
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                othelloGame.currentState.table[i][j] = -1;
        int expectedOutput = -1;

        // Act
        int output = othelloGame.get_next_move();
        // Assert
        REQUIRE(output == expectedOutput);
    }
    SECTION("Function getNextMove return -1 if table  have only 1 field value")
    {
        // Arrange
        GameSettings gameSettings = GameSettings();
        OthelloGame othelloGame = OthelloGame(gameSettings);
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                othelloGame.currentState.table[i][j] = -1;
        int expectedOutput = -1;

        // Act
        int output = othelloGame.get_next_move();
        // Assert
        REQUIRE(output == expectedOutput);
    }

}

