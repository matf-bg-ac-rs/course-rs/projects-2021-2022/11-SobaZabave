#define private public
#include "../../SobaZabave/src/Client/include/othellostate.h"
#include <QDebug>
#include "catch.hpp"

TEST_CASE("Othellostate consturctor", "[constructor]")
{
    SECTION("The Othello state constructor successfully set player1 on first move")
    {
        // Arrange
        Othellostate o;
        int expected = o.player1;
        // Act
        Othellostate othello = Othellostate();
        int output = othello.onMove;
        // Assert
        REQUIRE(output == expected);
    }
    SECTION("The Othello state constructor successfully does not throws exception when called")
    {
        // Arrange
        // Act
      //  Othellostate othello = Othellostate();
        // Assert
        REQUIRE_NOTHROW(Othellostate());
    }
    SECTION("The Othello state constructor successfully set zero on field that are not at the middle of board")
    {
        // Arrange
        Othellostate o;
        int expected = 0;
        // Act
        Othellostate othello = Othellostate();
        int output = othello.table[1][1];
        // Assert
        REQUIRE(output == expected);
    }


    SECTION("The Othello state constructor successfully set table[3][3] and table[4][4] on player1")
    {
        // Arrange
        Othellostate o;
        int expected = o.player1;
        // Act
        Othellostate othello = Othellostate();
        int outputTable1 = othello.table[3][3];
        int outputTabel2 = othello.table[4][4];
        // Assert
        CHECK(outputTable1 == expected);
        REQUIRE(outputTabel2 == expected);
    }
    SECTION("The Othello state constructor successfully set table[3][4] and table[4][3] on player2")
    {
        // Arrange
        Othellostate o;
        int expected = o.player2;
        // Act
        Othellostate othello = Othellostate();
        int outputTable1 = othello.table[3][4];
        int outputTabel2 = othello.table[4][3];
        // Assert
        CHECK(outputTable1 == expected);
        REQUIRE(outputTabel2 == expected);
    }
    SECTION("The Othello state constructor does not throw exception when called")
    {
        // Arrange
        Othellostate os;
        // Act

        // Assert
        REQUIRE_NOTHROW(os);
    }
}

TEST_CASE("Othellostate.copy constructor","[copy constructo]")
{
    SECTION("The Othello state copy constructor successfully set field on fifth row and sixth column on right field")
    {
        // Arrange
        int tableReturnedFromCopyConstructor;
        int expected = 1;
        // Act
        Othellostate othello = Othellostate();
        othello.table[5][6] = 1;
        Othellostate output = Othellostate(othello);
        tableReturnedFromCopyConstructor = output.table[5][6];
        // Assert
        REQUIRE(tableReturnedFromCopyConstructor == expected);
    }
    SECTION("The Othello state copy constructor does not throws exception when called ")
    {
        Othellostate os;

        REQUIRE_NOTHROW(Othellostate(os));
    }
}

TEST_CASE("Othellostate.isEnd","[function]"){

    SECTION("The Othello function isEnd successfully returns true if game has there is no zero on any field")
    {
        // Arrange
        bool expectedIfItIsEnd = true;
        int table[8][8];
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                table[i][j] = 1;
        // Act
        Othellostate othello = Othellostate();
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                othello.table[i][j] = table[i][j];
        bool output = othello.isEnd();
        // Assert
        REQUIRE(expectedIfItIsEnd == output);
    }
}

TEST_CASE("Othellostate.getRowAndCollumnFromMove","[function]")
{

    SECTION("The Othello function getRowAndCollumnFromMove successfully returns first and second digit of two digit number")
    {
        // Arrange
        int testNumber = 25;
        int expectedFirstDigit = 2;
        int expectedSecondDigit = 5;
        // Act
        Othellostate othello = Othellostate();
        QPair<int,int> output = othello.getRowAndCollumnFromMove(testNumber);
        // Assert
        CHECK(output.first == expectedFirstDigit);
        REQUIRE(output.second == expectedSecondDigit);
    }

    SECTION("The Othello function getRowAndCollumnFromMove successfully returns same number and zero if number is one digit number")
    {
        // Arrange
        int testNumber = 5;
        int expectedFirstDigit = 5;
        int expectedSecondDigit = 0;
        // Act
        Othellostate othello = Othellostate();
        QPair<int,int> output = othello.getRowAndCollumnFromMove(testNumber);
        // Assert
        CHECK(output.first == expectedSecondDigit);
        REQUIRE(output.second == expectedFirstDigit);
    }
}

TEST_CASE("Othellostate.putOnTable","[function]")
{

    SECTION("The Othello function putOnTable successfully put player that is on move on field calculated by function getRowAndCollumnFromMove")
    {
      // Arrange
      int move = 25;
      Othellostate othello = Othellostate();
      int onMove = othello.onMove;
      QPair<int,int> rowAndCollumnFromMove = othello.getRowAndCollumnFromMove(move);
      // Act
      othello.putOnTable(move);
      // Assert
      REQUIRE(othello.table[rowAndCollumnFromMove.first][rowAndCollumnFromMove.second] == onMove);

    }

}
TEST_CASE("Othellostate.getTableField","[function]")
{

    SECTION("The Othello function getTableField successfully returns value of field")
    {
      // Arrange
        QPair<int,int> fieldValue = {3,3};
        Othellostate othello = Othellostate();
        int expected = othello.player1;
      // Act

        int output = othello.getTableField(fieldValue.first, fieldValue.second);
      // Assert
        REQUIRE(output == expected);
    }


}
TEST_CASE("Othellostate.changeOnMove","[function]")
{

    SECTION("The Othello function changeOnMove successfully set move to opposite move")
    {
      // Arrange
        Othellostate othello = Othellostate();
        int expected = othello.onMove;
      // Act
        othello.changeOnMove();
        int output = othello.onMove;
      // Assert
        REQUIRE(output == (-1)*expected);
    }


}
