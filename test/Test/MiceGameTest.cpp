#define private public

#include "../../SobaZabave/src/Client/include/micegame.h"
#include "catch.hpp"
#include <iostream>

using namespace std;

TEST_CASE("MiceGame", "[class]")
{
    SECTION("MiceGame constructor creates an instance of MiceGame and initializes two players with ids 1 and 2 and sets the current player to player1")
    {
        // Arrange
        int player1ExpectedID = 1;
        int player2ExpectedID = 2;

        // Act
        MiceGame* miceGame = new MiceGame();
        Player* player1 = miceGame->player1();
        Player* player2 = miceGame->player2();
        Player* currentPlayer = miceGame->currentPlayer();
        int player1ID = player1->id();
        int player2ID = player2->id();

        // Assert
        REQUIRE_FALSE(miceGame == nullptr);
        REQUIRE_FALSE(player1 == nullptr);
        REQUIRE_FALSE(player2 == nullptr);
        REQUIRE(currentPlayer == player1);
        REQUIRE(player1ID == player1ExpectedID);
        REQUIRE(player2ID == player2ExpectedID);
    }

    SECTION("tryMove returns true for a valid field at the beginning of the game (current player is in phase0)"){

        // Arrange
        MiceGame* miceGame = new MiceGame();
        int position = 7;
        bool expectedResult = true;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns true if current player is in phase0 and the chosen field is not taken"){

        // Arrange
        MiceGame* miceGame = new MiceGame();
        miceGame->m_board.m_table[12] = 1;
        miceGame->m_board.m_table[13] = 2;
        int position = 7;
        bool expectedResult = true;

        // Act
        bool result = miceGame->tryMove(position);
        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns false if current player is in phase0 and the chosen field is already taken"){

        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->tryMove(12);
        miceGame->tryMove(13);

        int position = 13;
        bool expectedResult = false;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("try Move returns false if the chosen position is not valid (between 0 and 23)"){

        // Arrange
        MiceGame* miceGame = new MiceGame();

        int position = -7;
        bool expectedResult = false;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns false in phase 1 if the chosen source field is not current player's"){

        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_board.m_table[12] = 1;
        miceGame->m_board.m_emptyPositions.erase(12);
        miceGame->m_board.m_table[13] = 2;
        miceGame->m_board.m_emptyPositions.erase(13);
        bool expectedResult = false;
        miceGame->m_currentPlayer = miceGame->player1();
        miceGame->m_currentPlayer->m_phase = MicePlayer::Phase::Phase1;
        int position = 13;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns false in phase 2 if the chosen source field is not current player's"){

        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_board.m_table[12] = 1;
        miceGame->m_board.m_emptyPositions.erase(12);
        miceGame->m_board.m_table[13] = 2;
        miceGame->m_board.m_emptyPositions.erase(13);
        bool expectedResult = false;
        miceGame->m_currentPlayer = miceGame->player1();
        miceGame->m_currentPlayer->m_phase = MicePlayer::Phase::Phase2;
        int position = 13;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns false in phase 2 if the chosen source field is not current player's"){
        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_board.m_table[12] = 1;
        miceGame->m_board.m_emptyPositions.erase(12);
        miceGame->m_board.m_table[13] = 2;
        miceGame->m_board.m_emptyPositions.erase(13);

        miceGame->m_moveIndicator = false;
        bool expectedResult = false;
        miceGame->m_currentPlayer = miceGame->player1();
        miceGame->m_currentPlayer->m_phase = MicePlayer::Phase::Phase2;
        int position = 13;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns true in phase 1 if move indicator is true and the chosen destination field is empty and neighbor of source field"){
        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_moveIndicator = true;
        bool expectedResult = true;
        miceGame->m_currentPlayer = miceGame->player1();
        miceGame->m_currentPlayer->m_phase = MicePlayer::Phase::Phase1;
        miceGame->m_board.m_table[1] = 1;
        miceGame->m_board.m_emptyPositions.erase(1);
        miceGame->m_source = 1;
        int position = 2;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns true in phase 2 if move indicator is true and the chosen destination field is empty"){
        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_moveIndicator = true;
        bool expectedResult = true;
        miceGame->m_currentPlayer = miceGame->player1();
        miceGame->m_currentPlayer->m_phase = MicePlayer::Phase::Phase2;
        miceGame->m_board.m_table[1] = 1;
        miceGame->m_board.m_emptyPositions.erase(1);
        miceGame->m_source = 1;
        int position = 13;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns false in phase 1 if move indicator is true and the chosen destination field is not empty"){
        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_moveIndicator = true;
        bool expectedResult = false;
        miceGame->m_currentPlayer = miceGame->player1();
        miceGame->m_currentPlayer->m_phase = MicePlayer::Phase::Phase1;
        miceGame->m_board.m_table[1] = 1;
        miceGame->m_board.m_emptyPositions.erase(1);
        miceGame->m_board.m_table[2] = 2;
        miceGame->m_board.m_emptyPositions.erase(2);

        miceGame->m_source = 1;
        int position = 2;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns false in phase 1 if move indicator is true and the chosen destination field is not neighbor to source"){
        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_moveIndicator = true;
        bool expectedResult = false;
        miceGame->m_currentPlayer = miceGame->player1();
        miceGame->m_currentPlayer->m_phase = MicePlayer::Phase::Phase1;
        miceGame->m_board.m_table[1] = 1;
        miceGame->m_board.m_emptyPositions.erase(1);
        miceGame->m_board.m_table[2] = 2;
        miceGame->m_board.m_emptyPositions.erase(2);

        miceGame->m_source = 1;
        int position = 4;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns false in phase 2 if move indicator is true and the chosen destination field is not empty"){
        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_moveIndicator = true;
        bool expectedResult = false;
        miceGame->m_currentPlayer = miceGame->player1();
        miceGame->m_currentPlayer->m_phase = MicePlayer::Phase::Phase2;
        miceGame->m_board.m_table[1] = 1;
        miceGame->m_board.m_emptyPositions.erase(1);
        miceGame->m_board.m_table[13] = 2;
        miceGame->m_board.m_emptyPositions.erase(13);

        miceGame->m_source = 1;
        int position = 13;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryMove returns false in phase 2 if move indicator is true and the chosen destination field is not empty"){
        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_moveIndicator = true;
        bool expectedResult = false;
        miceGame->m_currentPlayer = miceGame->player1();
        miceGame->m_currentPlayer->m_phase = MicePlayer::Phase::Phase2;
        miceGame->m_board.m_table[1] = 1;
        miceGame->m_board.m_emptyPositions.erase(1);
        miceGame->m_board.m_table[13] = 2;
        miceGame->m_board.m_emptyPositions.erase(13);

        miceGame->m_source = 1;
        int position = 13;

        // Act
        bool result = miceGame->tryMove(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryEat returns false if the chosen field to eat is not opponents field"){
        // Arrange
        MiceGame* miceGame = new MiceGame();


        miceGame->m_board.m_table[12] = 1;
        int position = 12;
        miceGame->m_currentPlayer = miceGame->m_player1;
        bool expectedResult = false;

        // Act
        bool result = miceGame->tryEat(position);

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("tryEat returns false if the chosen field forms a mill, but the opponent has more than those 3 fields in a mill"){
        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_board.m_table[0] = 1;
        miceGame->m_board.m_table[1] = 1;
        miceGame->m_board.m_table[2] = 1;
        miceGame->m_board.m_table[5] = 1;

        int position = 2;
        miceGame->m_currentPlayer = miceGame->m_player2;
        bool expectedResult = false;

        // Act
        bool result = miceGame->tryEat(position);

        // Assert
        REQUIRE(result == expectedResult);
    }


    SECTION("tryEat sets the chosen position to empty if the chosen field is opponent's and the chosen field is put in the empty positions set"){
        // Arrange
        MiceGame* miceGame = new MiceGame();

        miceGame->m_board.m_table[1] = 1;

        int position = 1;
        miceGame->m_currentPlayer = miceGame->m_player2;

        // Act
        bool result = miceGame->tryEat(position);

        // Assert
        REQUIRE(miceGame->m_board.m_table[position] == 0);
        REQUIRE(miceGame->m_board.m_emptyPositions.find(position) != miceGame->m_board.m_emptyPositions.end());
    }

    SECTION("checkIsOver returns false if both of the players have more than 3 fields"){
        // Arrange
        MiceGame* miceGame = new MiceGame();
        bool expectedResult = false;

        miceGame->m_player1->setNumOfFigures(5);
        miceGame->m_player2->setNumOfFigures(6);

        // Act
        bool result = miceGame->checkIsOver();

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("checkIsOver returns true if one of the players has 2 fields"){
        // Arrange
        MiceGame* miceGame = new MiceGame();
        bool expectedResult = true;

        miceGame->m_player1->setNumOfFigures(2);
        miceGame->m_player2->setNumOfFigures(6);

        // Act
        bool result = miceGame->checkIsOver();

        // Assert
        REQUIRE(result == expectedResult);
    }

    SECTION("checkIsOver returns true if both of the players have 3 fields"){
        // Arrange
        MiceGame* miceGame = new MiceGame();
        bool expectedResult = true;

        miceGame->m_player1->setNumOfFigures(3);
        miceGame->m_player2->setNumOfFigures(3);

        // Act
        bool result = miceGame->checkIsOver();

        // Assert
        REQUIRE(result == expectedResult);
    }
}

