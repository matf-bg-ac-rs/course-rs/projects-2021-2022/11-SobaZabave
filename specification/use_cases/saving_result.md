# Saving result

**Description**: 
• Short description: After the game is played, the player enters his data and the application stores the final result.

**Actors**:
	-  /

**Preconditions**: The application is running, the player has played a game and the main menu is displayed.

**Postconditions**:  Information about the game played is saved.

**Main scenario**:
1. The application displays a name dialog.


2. The player enters his name.


3. The application stores the player's name and points won.


4. The application displays the main menu.


**Alternative scenarios**: Unexpected application exit. If in any step the user turns off the application, all stored information about the current game is canceled and the application ends. The use case is over.

**Subscenarios**: /

**Special requirements**: /

**Additional information**: / 	
