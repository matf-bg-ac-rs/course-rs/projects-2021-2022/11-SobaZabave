# Configuration of one game

**Description** : Configuration of difficulty of one game party, timer and
general rules like music, who starts the game, types of hints for both
games.

**Actors**: The Player : chooses difficulty, how long one game party last,
how many tokens will players lose if they use hints.

**Preconditions**: Application is running.

**Postconditions**: Game settings are completed and player can start the
game.

**Main scenario**:

    1. The player clicks on "Settings" button.
    2. Application opens settings window.
    3. The player chooses game difficulty by choosing number from one to ten by repeatedly clicking on "+" button or "-".
    4. The player chooses available time for one game party. How many tokens will winner get depends on this timer.
    5. The player can choose songs that will play during game party.
    6. Player who initiated playing one game party chooses if he is black or white player.
    7. Player clicks "Home" button and he is returned to Main Room of Application.

**Alternative scenarios:**

Unexpected application exit: The game is discarded and the application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
