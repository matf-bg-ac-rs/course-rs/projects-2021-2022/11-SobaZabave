# Choosing one game

**Description:** The player chooses which game he will play, whether he is creating a game, joining a game or playing a single player game.

**Actors:**
Player - chooses the game

**Preconditions:** The application is running.

**Postconditions:** Game choosing is completed and the player can start the game.

**Main scenario:**
1. The player clicks on *Choose game* button in main window.
2. The application opens the *Choose game* window.
3. The player chooses which one of two games he wants to play by clicking one of two possible game buttons: *Othello* or *Mice*.
4. The player chooses if he wants to create a new multiplayer game party or he wants to join the existing game party by a link or to play single player by clicking one of three possible buttons.
5. If the player chose to create multiplayer game, game link is generated and he sends it to another player.
6. If the player chose to join the existing game, he enters the link he got from another player.
7. Game choosing is done and the player can click *Start* to play the chosen game.
8. If the player chose game Othello, application moves to *Playing one game party of Othello* use case.
9. If the player chose game Mice, application moves to *Playing one game party of Mice* use case.
	
**Alternative scenarios**:
Unexpected application exit: The game choosing is discarded and the
application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
