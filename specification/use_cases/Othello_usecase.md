# Playing one game party _Othello_

**Description**: The player starts the game from the main menu. The application loads game settings and starts the game. When the game is over, application stores information about the game and returns tokens, player result and list of the best results.

**Actors**:
	- The Player: plays one game.

**Preconditions**: The application is launched, all settings are chosen and Othello game is selected.

**Postconditions**: The game is finished, all game data are saved, tokens and rang list is updated.

**Main scenario**:

1. The Player starts the game by pressing the *"Start"* button.
2. The application loads all game settings.
3. The application creates TableState and starts new game.
4. While the game is not finished, repeated steps are: <br />
    1. If the table is full, game is finished and goes to step 5. <br />
    2. If the Player can't put any disc on table his move is over and goes to step 4.6. <br />
    3. Application shows all possible fields where Player can put disc <br />
    4. Application checks if time for one move is over <br />
        1. If time is over application makes random move and goes to step 4.6. <br />
    5. Player put disc on table <br/>
        1. If Player clicks "show hint" button his tokens are updated and hint for next move is shown <br /> 
        2. Goes to 4.6. <br />
    6. Player finished his move, application changes state of table by flipping certain discs <br />
        1. If game is multiplayer application sends Player's move to opponent <br />
        2. If game is singleplayer application calculates next computers move depanding on settings <br />
    7. Application gets opponents next move, updates current table state and repeats step 4. <br />
5. The application shows the result of the game
6. The application shows the won tokens
7. The Game moves to the use-case *"Saving results"*, when its over move to step 8
8. The application shows main menu


**Alternative scenarios**:

- A1: **Unexpected application exit**: The game is discarded and the
application closes.

**Subscenarios**: None.

**Special requirements**: If game is multiplayer game server should be up.

**Additional information**: During the game, the application shows current result of game and timer for move.







