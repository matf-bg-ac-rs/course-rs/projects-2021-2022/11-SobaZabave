#include "include/server.h"
Server::Server(QObject *parent) :
    QObject(parent)
{
    m_client1 = nullptr;
    m_client2 = nullptr;

    server = new QTcpServer(this);

    connect(server, &QTcpServer::newConnection,
            this, &Server::newConnection);


    if(!server->listen(QHostAddress::LocalHost, 12345))
    {
        qDebug() << "Server could not start";
    }
    else
    {
        qDebug() << "Server started!";
    }
}

Server::~Server(){
    delete server;
}

void Server::newConnection()
{
    qInfo() << m_client1;

    QTcpSocket *socket = server->nextPendingConnection();
    bool ind = false;
    if(socket)
    {
       if(m_client1 == nullptr){
            ind = true;
            m_client1 = socket;
        }
        else{
            m_client2 = socket;
        }

        connect(socket ,&QTcpSocket::readyRead,this,&Server::on_readyRead);
        connect(socket ,&QTcpSocket::disconnected,socket ,&QTcpSocket::deleteLater);
    }

    if(ind)
        socket->write("You are client1");
    else
        socket->write("You are client2");

    socket->flush();
    socket->waitForBytesWritten(3000);

}

void Server::on_readyRead()
{
    QTcpSocket * senderSocket = dynamic_cast<QTcpSocket*>(sender());
    if(senderSocket == m_client1 && m_client2 != nullptr)
        m_client2->write(senderSocket->readAll());
    else
        m_client1->write(senderSocket->readAll());

}

