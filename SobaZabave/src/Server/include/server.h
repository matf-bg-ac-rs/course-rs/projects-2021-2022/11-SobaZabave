#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>
#include <QDebug>

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);
    ~Server();
signals:

public slots:
    void newConnection();
    void on_readyRead();

private:
    QTcpServer *server;
    QTcpSocket* m_client1;
    QTcpSocket* m_client2;
};

#endif // SERVER_H
