#ifndef MICEFIELD_H
#define MICEFIELD_H

#include "include/field.h"

class MiceField : public Field {
public:
  MiceField(QPointF position);
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
             QWidget *widget) override;
};

#endif // MICEFIELD_H
