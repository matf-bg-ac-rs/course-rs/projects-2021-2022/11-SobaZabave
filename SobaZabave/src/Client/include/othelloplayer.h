#ifndef OTHELLOPLAYER_H
#define OTHELLOPLAYER_H

#include "../include/player.h"

class OthelloPlayer : public Player {
public:
  OthelloPlayer(int id);
};

#endif // OTHELLOPLAYER_H
