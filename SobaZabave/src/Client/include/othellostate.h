#ifndef OTHELLOSTATE_H
#define OTHELLOSTATE_H

#include <QVector>
#include <iomanip>
#include <iostream>

class Othellostate {
public:
  Othellostate();
  Othellostate(const Othellostate &os);
  ~Othellostate();

  void putOnTable(int move);
  void changeOnMove();
  int getTableField(int row, int column) const;
  int getOnMove() const;
  bool isEnd() const;
  const int player1 = 1;
  const int player2 = -1;
  const int TABLEDIMENSION = 8;

private:
  int table[8][8] = {0};
  int onMove;
  QPair<int, int> getRowAndCollumnFromMove(int move);
};

#endif // OTHELLOSTATE_H
