#ifndef OTHELLOMINMAX_H
#define OTHELLOMINMAX_H

#include "../include/gamesettings.h"
#include "../include/othellostate.h"
#include "../include/othellostatecontroller.h"
#include <QPair>
#include <limits.h>
#include <utility>

class OthelloMinMax {
public:
  OthelloMinMax(GameSettings &gs);

  int getNextAIMove(const Othellostate &os) const;

private:
  int Evaluate(const Othellostate &os) const;
  int HeuristicScore(const Othellostate &os) const;
  int getHeuristicScoreOnField(int row, int column) const;

  QPair<int, int> Min(const Othellostate &os, int depth, int alpha,
                      int beta) const;
  QPair<int, int> Max(const Othellostate &os, int depth, int alpha,
                      int beta) const;

  GameSettings::GameDifficulty gameDifficulty;
  int difficulty;
  const int TABLEDIMENSION = 8;
  const int scoreBoard[8][8] = {{500, -150, 30, 10, 10, 30, -150, 500},
                                {-150, -250, 0, 0, 0, 0, -250, -150},
                                {30, 0, 1, 2, 2, 1, 0, 30},
                                {10, 0, 2, 16, 16, 2, 0, 30},
                                {10, 0, 2, 16, 16, 2, 0, 30},
                                {30, 0, 1, 2, 2, 1, 0, 30},
                                {-150, -250, 0, 0, 0, 0, -250, -150},
                                {500, -150, 30, 10, 10, 30, -150, 500}};
};

#endif // OTHELLOMINMAX_H
