#ifndef OTHELLO_H
#define OTHELLO_H

#include "othelloboardscene.h"
#include "othellogame.h"
#include <QGraphicsItem>
#include <QWidget>

namespace Ui {
class Othello;
}

class OthelloGameWindow : public QWidget {
  Q_OBJECT

public:
  OthelloGameWindow(QWidget *parent, GameSettings &gs);
  ~OthelloGameWindow() override;

  void drawGuiTable(const Othellostate &os);
  void setUsername(QString username);
  OthelloGame *othelloGame() const;

signals:
  void BackClicked();

private slots:
  void on_homeButtonOthelloPlay_clicked();

public slots:
  void onFieldClick(QPointF pos);

private:
  Ui::Othello *ui;
  OthelloGame *m_othelloGame;
  OthelloBoardScene *m_boardScene;

  const qreal offset = 450 / 8.0;

  const int INVALID_ITEM = -100;

  OthelloGameWindow(const OthelloGameWindow &) = delete;
  OthelloGameWindow &operator=(const OthelloGameWindow &) = delete;
  OthelloGameWindow(OthelloGameWindow &&) = delete;
  OthelloGameWindow &operator=(OthelloGameWindow &&) = delete;

  int getFieldIndexByPosition(Field *fieldItem) const;
  int findMoveForOthelloFromPositionOfField(QPointF pos);
  int getFormattedMoveFromFieldIndex(int fieldIndex);
};

#endif // OTHELLO_H
