#ifndef PLAYER_H
#define PLAYER_H

#include <QString>

class Player {

public:
  Player(int id);

  int id() const;
  void setId(int id);

  QString username() const;
  void setUsername(const QString &username);

  int numOfFigures() const;
  void setNumOfFigures(int numOfFigures);

protected:
  int m_id;
  QString m_username;
  int m_numOfFigures;
};

#endif // PLAYER_H
