#ifndef SETTINGS_H
#define SETTINGS_H

#include "gamesettings.h"
#include <QWidget>

namespace Ui {
class Settings;
}

class Settings : public QWidget {
  Q_OBJECT

public:
  explicit Settings(QWidget *parent = nullptr);
  ~Settings();
  void setUsername(QString username);

private:
  Ui::Settings *ui;
  GameSettings::GameDifficulty gameDifficuly;
  int difficultyLevel;

signals:

  void BackClicked();
  void DifficultySetClicked(const GameSettings::GameDifficulty &gd);

private slots:

  void on_backButtonSettings_clicked();
  void on_difficultyButton_clicked();
  void on_spinBox_valueChanged(const QString &arg1);
};

#endif // SETTINGS_H
