#ifndef FIELD_H
#define FIELD_H

#include <QGraphicsItem>
#include <QPainter>
#include <QPointF>

class Field : public QGraphicsItem {
public:
  Field(QPointF m_position);
  ~Field();

  enum class FieldState { Empty, Player1, Player2 };

  QRectF boundingRect() const override;
  inline qint32 Size() const { return 50; }

  Field::FieldState currentFieldState() const;
  void setCurrentFieldState(Field::FieldState currentFieldState);

  QPointF position() const;

protected:
  FieldState m_currentFieldState = FieldState::Empty;
  QPointF m_position;
};

#endif // FIELD_H
