#ifndef OTHELLOFIELD_H
#define OTHELLOFIELD_H

#include "../include/field.h"

class OthelloField : public Field {
public:
  OthelloField(QPointF position);
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
             QWidget *widget) override;
};

#endif // OTHELLOFIELD_H
