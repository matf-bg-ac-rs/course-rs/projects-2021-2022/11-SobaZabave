#ifndef OTHELLOGAME_H
#define OTHELLOGAME_H

#include "../include/gamesettings.h"
#include "../include/othellominmax.h"
#include "../include/othelloplayer.h"
#include "../include/othellostatecontroller.h"
#include "othellostate.h"
#include <QMessageBox>
#include <QPair>
#include <stdlib.h>
#include <unistd.h>

class OthelloGame {
public:
  OthelloGame(GameSettings &gs);
  ~OthelloGame();

  void doMove(int move);
  QPair<int, int> calculatePlayerFields() const;
  int get_next_move();

  int getWinner() const;
  int getDifficulty() const;
  GameSettings getGameSettings();
  void showMessageBox(QString content) const;

  GameSettings::GameType typeOfGame;
  Othellostate currentState;

  OthelloPlayer *player1 = new OthelloPlayer(1);
  OthelloPlayer *player2 = new OthelloPlayer(-1);

  const int boardDimension = 8;
  const int CANT_MOVE = -1;

  OthelloGame(const OthelloGame &) = delete;
  OthelloGame &operator=(const OthelloGame &) = delete;
  OthelloGame(OthelloGame &&) = delete;
  OthelloGame &operator=(OthelloGame &&) = delete;

private:
  OthelloMinMax AI;
  GameSettings gameSettings;
};

#endif // OTHELLOGAME_H
