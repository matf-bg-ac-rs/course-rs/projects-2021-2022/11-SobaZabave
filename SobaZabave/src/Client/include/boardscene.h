#ifndef BOARDSCENE_H
#define BOARDSCENE_H

#include "field.h"
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QObject>
#include <QPointF>
#include <QVector>

class BoardScene : public QGraphicsScene {
  Q_OBJECT

signals:
  void signalClickedSomething(QPointF);

protected:
  virtual void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) override;

public:
  BoardScene(QObject *parent = nullptr);
  ~BoardScene();

  QPointF fieldPosition(const int index) const;

  virtual void addAllFields(qreal viewWidth, qreal viewHeight,
                            qreal offset) = 0;
  virtual void addAllLines(qreal offset, qreal viewWidth, qreal viewHeight) = 0;

  const QVector<Field *> &fields() const;

  QVector<Field *> m_fields;
  QVector<QGraphicsLineItem *> m_lines;
};

#endif // BOARDSCENE_H
