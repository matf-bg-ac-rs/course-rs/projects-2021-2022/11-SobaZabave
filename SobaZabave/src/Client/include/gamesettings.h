#ifndef GAMESETTINGS_H
#define GAMESETTINGS_H

class GameSettings {
public:
  enum class GameDifficulty { EASY, MEDIUM, HARD, VERYHARD };

  enum class GameType { SINGLEPLAYER, LOCAL, SERVER };

  GameSettings();
  GameSettings(GameSettings &gs);

  GameDifficulty getdifficultyLevel();
  GameType getgameType();
  void setDifficultyLevel(const GameDifficulty difficultyLevel);
  void setGameType(const GameType gameType);

private:
  GameDifficulty difficultyLevel;
  GameType gameType;
};

#endif // GAMESETTINGS_H
