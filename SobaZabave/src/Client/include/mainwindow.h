#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "gamesettings.h"
#include "micegamewindowlocal.h"
#include "micegamewindowserver.h"
#include "othellogamewindow.h"
#include "settings.h"
#include <QMessageBox>
#include <QWidget>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QtSql>

#include "include/othellogame.h"
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QWidget {
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:

  void on_logInButton_clicked();
  void on_radioButtonMPMiceGame_clicked();
  void on_radioButtonLocalMiceGame_clicked();
  void on_instructionsButton_clicked();
  void on_backButtonCG_clicked();
  void on_othelloButtonCG_clicked();
  void on_miceButtonCG_clicked();
  void on_settingsButton_clicked();
  void on_backButtonOG_clicked();
  void on_playButtonOthello_clicked();
  void on_backButtonMice_clicked();
  void on_playButtonMice_clicked();
  void on_backButtonInstructions_clicked();
  void on_radioButtonLocalOthelloGame_clicked();
  void on_radioButtonSingleOthelloGame_clicked();

public slots:
  void on_setDifficulty(GameSettings::GameDifficulty gameDifficulty);
  void moveHomeFromSettings();
  void moveHomeFromMiceLocal();
  void moveHomeFromMiceMultiplayer();
  void moveHomeFromOthello();

private:
  Ui::MainWindow *ui;
  GameSettings gameSettings;
  Settings *_settings = nullptr;
  MiceGameWindow *_miceGameWindow = nullptr;
  OthelloGameWindow *_othelloGameWindow = nullptr;
  bool multiPlayerMiceGame = false;
  bool localPlayerMiceGame = false;
  bool localOthelloGame = false;
  bool singlePlayerOthelloGame = false;
  QString username = "";
  void showMessageBox(QString content) const;
};
#endif // MAINWINDOW_H
