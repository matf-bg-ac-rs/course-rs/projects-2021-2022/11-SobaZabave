﻿#ifndef MICEGAME_H
#define MICEGAME_H

#include "miceboard.h"
#include "miceplayer.h"
#include <QMessageBox>
#include <QVector>
#include <algorithm>
#include <iostream>

using namespace std;

class MiceGame {
public:
  enum class GameResult {
    Draw = 0,
    WinPlayer1 = 1,
    WinPlayer2 = 2,
    NotOver = -1
  };

  MiceGame();
  ~MiceGame();
  MiceGame(const MiceGame &) = delete;
  MiceGame &operator=(const MiceGame &) = delete;
  MiceGame(MiceGame &&) = delete;
  MiceGame &operator=(MiceGame &&) = delete;

  bool tryMove(const int position);
  bool tryEat(const int position);
  bool checkIsOver() const;
  void showMessageBox(QString content) const;

  const MiceBoard &board() const;
  bool eatIndicator() const;
  int source() const;
  int destination() const;
  MicePlayer *currentPlayer() const;
  MicePlayer *player1() const;
  MicePlayer *player2() const;

  void setPlayer1(MicePlayer *player1);
  void setPlayer2(MicePlayer *player2);
  void setCurrentPlayer(MicePlayer *currentPlayer);
  void setEatIndicator(bool eatIndicator);

private:
  void playMove();
  bool eatIfYouCan(const int position);
  void tryToChangePhase();
  bool tryMovePhase1or2(int position);

  void setMoveIndicator(bool value);
  void setSource(int value);
  void setDestination(int value);

  MicePlayer *otherPlayer() const;
  MicePlayer::Phase currentPlayerPhase() const;
  int currentPlayerId() const;

  MicePlayer *m_player1;
  MicePlayer *m_player2;
  MicePlayer *m_currentPlayer;
  bool m_moveIndicator;
  bool m_eatIndicator = false;

  int m_source;
  int m_destination;

  void playMovePhase0();
  void playMovePhase1And2();
  bool wonPlayer2() const;
  bool wonPlayer1() const;
  bool isDraw() const;
  bool formsMillOnEvenPosition(const int position, const int square) const;
  bool checkForSquare(const int position, const int square) const;
  int getPositionSquare(const int position) const;
  bool formsMillOnOddPosition(const int position, const int square) const;

  GameResult getWinner() const;

  bool canEatAtThisPos(const int position) const;
  bool formsMill(const int position) const;

  MiceBoard m_board;
  int m_numOfMoves = 0;
  int numOfPlayers = 2;
  int numOfPieces = 9;
};

#endif // MICEGAME_H
