#ifndef MICEGAMEWINDOW_H
#define MICEGAMEWINDOW_H

#include "field.h"
#include "miceboardscene.h"
#include "micegame.h"
#include <QGraphicsItem>
#include <QWidget>

namespace Ui {
class MiceGameWindow;
}

class MiceGameWindow : public QWidget {
  Q_OBJECT

public:
  MiceGameWindow(QWidget *parent = nullptr);
  virtual ~MiceGameWindow();
  void setUsername(QString username);

public slots:
  virtual void onFieldClick(QPointF) = 0;

signals:
  void BackClicked();

protected slots:
  void on_btBack_clicked();

protected:
  Ui::MiceGameWindow *ui;
  MiceGame *m_miceGame;
  MiceBoardScene *m_boardScene;

  const qreal offset = 25.0;

  MiceGameWindow(const MiceGameWindow &) = delete;
  MiceGameWindow &operator=(const MiceGameWindow &) = delete;
  MiceGameWindow(MiceGameWindow &&) = delete;
  MiceGameWindow &operator=(MiceGameWindow &&) = delete;

  int getFieldIndexByPosition(MiceField *fieldItem) const;

  MiceGame *miceGame() const;
  int getFieldIndexByPosition(QPointF pos) const;

  bool eatAndUpdateBoard(MiceField *fieldItem, const int index);
  bool moveAndUpdateBoard(const int index);

private:
};

#endif // MICEGAMEWINDOW_H
