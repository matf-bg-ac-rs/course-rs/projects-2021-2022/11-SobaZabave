#ifndef MICE_H
#define MICE_H

#include <QWidget>

namespace Ui {
class Mice;
}

class Mice : public QWidget {
  Q_OBJECT

public:
  explicit Mice(QWidget *parent = nullptr);
  ~Mice();

private:
  Ui::Mice *ui;

signals:
  void HomeClicked();
private slots:
  void on_pushButton_clicked();
};

#endif // MICE_H
