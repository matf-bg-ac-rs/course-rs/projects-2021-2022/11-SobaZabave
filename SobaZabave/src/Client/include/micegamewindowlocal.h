#ifndef MICEGAMEWINDOWLOCAL_H
#define MICEGAMEWINDOWLOCAL_H

#include "field.h"
#include "miceboardscene.h"
#include "micegame.h"
#include "micegamewindow.h"
#include "ui_micegamewindow.h"
#include <QGraphicsItem>
#include <QWidget>

namespace Ui {
class MiceGameWindow;
}

class MiceGameWindowLocal : public MiceGameWindow {
  Q_OBJECT

public:
  MiceGameWindowLocal(QWidget *parent = nullptr);
  ~MiceGameWindowLocal();

public slots:
  void onFieldClick(QPointF) override;
};

#endif // MICEGAMEWINDOWLOCAL_H
