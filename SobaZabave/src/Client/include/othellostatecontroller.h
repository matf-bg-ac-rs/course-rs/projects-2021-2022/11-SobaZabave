#ifndef OTHELLOSTATECONTROLLER_H
#define OTHELLOSTATECONTROLLER_H

#include "othellostate.h"
#include <QPair>

namespace OthelloStateController {

void playMove(Othellostate &os, int move);
void playNoMove(Othellostate &os);
bool hasNextMove(const Othellostate &os);
QVector<int> getNextValidMoves(const Othellostate &os);
bool isPossibleMove(const Othellostate &os, int row, int col);
QPair<int, int> getRowAndCollumnFromMove(int move);
} // namespace OthelloStateController

#endif // OTHELLOSTATECONTROLLER_H
