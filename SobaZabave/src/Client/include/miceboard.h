#ifndef MICEBOARD_H
#define MICEBOARD_H

#include <QPair>
#include <QVector>
#include <iostream>
#include <unordered_set>

using namespace std;

class MiceBoard {
public:
  MiceBoard();
  ~MiceBoard();

  const QVector<int> &getTable() const;
  const QVector<QPair<int, int>> &getGraph() const;
  unordered_set<int> &getEmptyPositions();
  int getNumberOfFields() const;

  bool checkIsValidMovePhase0and2(const int move) const;
  bool checkIsValidMovePhase1(const QPair<int, int> &move,
                              const int player) const;

  void setTable(const int destination, const int value);

  MiceBoard(const MiceBoard &) = delete;
  MiceBoard &operator=(const MiceBoard &) = delete;
  MiceBoard(MiceBoard &&) = delete;
  MiceBoard &operator=(MiceBoard &&) = delete;

private:
  const int numberOfFields = 24;
  const int emptyField = 0;

  QVector<int> m_table;
  QVector<QPair<int, int>> m_graph;

  unordered_set<int> m_emptyPositions;

  const unordered_set<int> &getValidMovementsPhase0and2() const;
  const QVector<QPair<int, int>>
  getValidMovementsPhase1(const int player) const;

  void initializePositions();
  void initializeGraph();
};

#endif // MICEBOARD_H
