#ifndef CLIENT_H
#define CLIENT_H

#include <QAbstractSocket>
#include <QDebug>
#include <QHostAddress>
#include <QObject>
#include <QTcpSocket>

class Client : public QObject {
  Q_OBJECT
public:
  explicit Client(QObject *parent = 0);
  ~Client();
  bool yourTurn;
  bool doConnect();

signals:
  void sendIndexToWindow(int index);

public slots:
  void readIndexFromWindow(int index);
  void connected();
  void disconnected();
  void bytesWritten(qint64 bytes);
  void readyRead();
  bool writeData(QByteArray data);

private:
  QTcpSocket *socket;
  bool client1_ind;
  bool client2_ind;
};

#endif // CLIENT_H
