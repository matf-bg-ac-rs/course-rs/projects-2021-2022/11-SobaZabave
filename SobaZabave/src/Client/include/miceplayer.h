#ifndef MICEPLAYER_H
#define MICEPLAYER_H

#include "../include/player.h"
#include <QString>

class MicePlayer : public Player {
public:
  MicePlayer(int id);

  enum class Phase { Phase0, Phase1, Phase2 };

  Phase phase() const;
  void setPhase(const Phase &phase);

  bool operator==(const MicePlayer &other) const;

private:
  Phase m_phase = Phase::Phase0;
};

#endif // MICEPLAYER_H
