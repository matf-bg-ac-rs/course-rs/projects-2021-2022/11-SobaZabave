#ifndef MICEBOARDSCENE_H
#define MICEBOARDSCENE_H

#include "boardscene.h"
#include "micefield.h"
#include <QObject>
#include <QPointF>
#include <QVector>

class MiceBoardScene : public BoardScene {
public:
  MiceBoardScene(QObject *parent = nullptr);

  void addAllFields(qreal viewWidth, qreal viewHeight, qreal offset) override;
  void addAllLines(qreal offset, qreal viewWidth, qreal viewHeight) override;

private:
  const int numOfFields = 24;
};

#endif // MICEBOARDSCENE_H
