#ifndef MICEGAMEWINDOWSERVER_H
#define MICEGAMEWINDOWSERVER_H

#include "client.h"
#include "field.h"
#include "miceboardscene.h"
#include "micegame.h"
#include "micegamewindow.h"
#include <QGraphicsItem>
#include <QWidget>

namespace Ui {
class MiceGameWindow;
}

class MiceGameWindowServer : public MiceGameWindow {
  Q_OBJECT

signals:
  void sendIndexToClient(int index);

public slots:
  void readIndexFromClient(int index);
  void onFieldClick(QPointF) override;

public:
  MiceGameWindowServer(QWidget *parent = nullptr);
  ~MiceGameWindowServer() override;

private:
  Client *m_tcpclient;
  MiceField *fieldItem;
};

#endif // MICEGAMEWINDOWSERVER_H
