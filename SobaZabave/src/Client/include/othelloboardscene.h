#ifndef OTHELLOBOARDSCENE_H
#define OTHELLOBOARDSCENE_H

#include "boardscene.h"
#include "othellofield.h"
#include <QGraphicsScene>
#include <QObject>
#include <QPointF>
#include <QVector>

class OthelloBoardScene : public BoardScene {
public:
  OthelloBoardScene(QObject *parent = nullptr);

  void addAllFields(qreal viewWidth, qreal viewHeight, qreal offset) override;
  void addAllLines(qreal viewWidth, qreal viewHeight, qreal offset) override;

  const int numOfFields = 64;
  const int boardDimension = 8;

private:
  const int detailedPositioning = 3.6;
};

#endif // OTHELLOBOARDSCENE_H
