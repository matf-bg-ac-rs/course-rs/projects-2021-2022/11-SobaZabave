QT       += core gui
 QT += core
QT += sql
QT += network


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/boardscene.cpp \
    src/client.cpp \
    src/field.cpp \
    src/gamesettings.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/miceboard.cpp \
    src/miceboardscene.cpp \
    src/micefield.cpp \
    src/micegame.cpp \
    src/micegamewindow.cpp \
    src/micegamewindowlocal.cpp \
    src/micegamewindowserver.cpp \
    src/miceplayer.cpp \
    src/othelloboardscene.cpp \
    src/othellofield.cpp \
    src/othellogame.cpp \
    src/othellogamewindow.cpp \
    src/othellominmax.cpp \
    src/othelloplayer.cpp \
    src/othellostate.cpp \
    src/othellostatecontroller.cpp \
    src/player.cpp \
    src/settings.cpp


HEADERS += \
    include/boardscene.h \
    include/client.h \
    include/field.h \
    include/gamesettings.h \
    include/mainwindow.h \
    include/miceboard.h \
    include/miceboardscene.h \
    include/micefield.h \
    include/micegame.h \
    include/micegamewindow.h \
    include/micegamewindowlocal.h \
    include/micegamewindowserver.h \
    include/miceplayer.h \
    include/othelloboardscene.h \
    include/othellofield.h \
    include/othellogame.h \
    include/othellogamewindow.h \
    include/othellominmax.h \
    include/othelloplayer.h \
    include/othellostate.h \
    include/othellostatecontroller.h \
    include/player.h \
    include/settings.h

FORMS += \
    forms/mainwindow.ui \
    forms/micegamewindow.ui \
    forms/othellogamewindow.ui \
    forms/settings.ui



INCLUDEPATH += $$_PRO_FILE_PWD_

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
