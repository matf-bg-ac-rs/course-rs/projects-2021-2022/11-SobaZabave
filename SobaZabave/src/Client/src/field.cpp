#include "../include/field.h"

Field::Field(QPointF position) : QGraphicsItem(), m_position(position) {
  setPos(position);
}

Field::~Field() = default;

auto Field::boundingRect() const -> QRectF {
  return QRectF(0, 0, Size(), Size());
}

auto Field::currentFieldState() const -> Field::FieldState {
  return m_currentFieldState;
}

void Field::setCurrentFieldState(Field::FieldState currentFieldState) {
  m_currentFieldState = currentFieldState;
}

auto Field::position() const -> QPointF { return m_position; }
