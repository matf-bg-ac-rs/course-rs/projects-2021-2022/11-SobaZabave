#include "../include/othellogamewindow.h"
#include "ui_othellogamewindow.h"

OthelloGameWindow::OthelloGameWindow(QWidget *parent, GameSettings &gs)
    : QWidget(parent), ui(new Ui::Othello),
      m_boardScene(new OthelloBoardScene(this)) {

  m_othelloGame = new OthelloGame(gs);
  setStyleSheet("background-image: url(:/resources/resources/back.jpg)");
  ui->setupUi(this);

  m_boardScene->setSceneRect(ui->othelloGraphicsView->rect());
  ui->othelloGraphicsView->setScene(m_boardScene);
  ui->othelloGraphicsView->setRenderHint(QPainter::Antialiasing);
  ui->othelloGraphicsView->setAlignment(Qt::AlignTop);

  ui->othelloGraphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  ui->othelloGraphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

  m_boardScene->addAllFields(ui->othelloGraphicsView->width(),
                             ui->othelloGraphicsView->height(), offset);

  connect(m_boardScene, &OthelloBoardScene::signalClickedSomething, this,
          &OthelloGameWindow::onFieldClick);
}

OthelloGameWindow::~OthelloGameWindow() {
  delete ui;
  delete m_boardScene;
  delete m_othelloGame;
}

auto OthelloGameWindow::othelloGame() const -> OthelloGame * {
  return m_othelloGame;
}

auto OthelloGameWindow::getFormattedMoveFromFieldIndex(int fieldIndex) -> int {
  int move = fieldIndex / m_othelloGame->boardDimension +
             (fieldIndex % m_othelloGame->boardDimension) * 10;
  return move;
}

auto OthelloGameWindow::findMoveForOthelloFromPositionOfField(QPointF pos)
    -> int {
  auto itemFromPOsitionOnBoard = m_boardScene->itemAt(pos, QTransform());

  if (itemFromPOsitionOnBoard == nullptr) {
    return INVALID_ITEM;
  }

  auto *fieldItem = static_cast<Field *>(itemFromPOsitionOnBoard);

  int fieldIndex = getFieldIndexByPosition(fieldItem);

  if (fieldIndex == INVALID_ITEM) {
    m_othelloGame->showMessageBox("Upozorenje, nepostojeci indeks polja!");
    return INVALID_ITEM;
  }

  int move = getFormattedMoveFromFieldIndex(fieldIndex);

  return move;
}

void OthelloGameWindow::onFieldClick(QPointF pos) {
  QVector<int> possible_moves =
      OthelloStateController::getNextValidMoves(m_othelloGame->currentState);
  if (possible_moves.length() == 0) {
    m_othelloGame->doMove(m_othelloGame->CANT_MOVE);
    return;
  }

  int move = findMoveForOthelloFromPositionOfField(pos);

  if (m_othelloGame->currentState.getTableField(move / 10, move % 10) != 0)
    return;

  if (!possible_moves.contains(move)) {
    m_othelloGame->showMessageBox("Ne mozete postaviti token na ovo polje!");
    return;
  }

  m_othelloGame->doMove(move);

  if (m_othelloGame->currentState.isEnd()) {
    drawGuiTable(m_othelloGame->currentState);
    if (m_othelloGame->getWinner() == m_othelloGame->player1->id())
      m_othelloGame->showMessageBox("Pobedio je ZUTI igrac!     ");
    else
      m_othelloGame->showMessageBox("Pobedio je ZELENI igrac!     ");
    return;
  }

  if (othelloGame()->typeOfGame == GameSettings::GameType::LOCAL) {
    drawGuiTable(m_othelloGame->currentState);
    return;
  }

  int next_move = m_othelloGame->get_next_move();
  m_othelloGame->doMove(next_move);
  drawGuiTable(m_othelloGame->currentState);

  if (m_othelloGame->currentState.isEnd()) {
    if (m_othelloGame->getWinner() == m_othelloGame->player1->id())
      m_othelloGame->showMessageBox("Pobedio je ZUTI igrac!     ");
    else
      m_othelloGame->showMessageBox("Pobedio je ZELENI igrac!     ");
    return;
  }
  return;
}

void OthelloGameWindow::drawGuiTable(const Othellostate &os) {
  auto field = static_cast<OthelloField *>(m_boardScene->m_fields[0]);
  int index;
  for (int i = 0; i < m_othelloGame->boardDimension; ++i) {
    for (int j = 0; j < m_othelloGame->boardDimension; ++j) {
      index = j * m_othelloGame->boardDimension + i;
      field = static_cast<OthelloField *>(m_boardScene->m_fields[index]);
      auto tableField = os.getTableField(i, j);
      if (tableField == m_othelloGame->player2->id()) {
        field->setCurrentFieldState(Field::FieldState::Player2);
      } else if (tableField == m_othelloGame->player1->id()) {
        field->setCurrentFieldState(Field::FieldState::Player1);
      } else {
        field->setCurrentFieldState(Field::FieldState::Empty);
      }
    }
  }
  ui->greenScoreLabel->clear();
  ui->yellowScoreLabel->clear();
  ui->yellowScoreLabel->setText(
      QString::number(m_othelloGame->player1->numOfFigures()));
  ui->greenScoreLabel->setText(
      QString::number(m_othelloGame->player2->numOfFigures()));
  ui->othelloGraphicsView->viewport()->update();
}

void OthelloGameWindow::setUsername(QString username) {
  ui->usernameLabelOthelloGame->setText(username);
}

auto OthelloGameWindow::getFieldIndexByPosition(Field *fieldItem) const -> int {
  for (int i = 0;
       i < m_othelloGame->boardDimension * m_othelloGame->boardDimension; ++i) {
    if (m_boardScene->fieldPosition(i) == fieldItem->pos()) {
      return i;
    }
  }
  return INVALID_ITEM;
}

void OthelloGameWindow::on_homeButtonOthelloPlay_clicked() {
  emit BackClicked();
}
