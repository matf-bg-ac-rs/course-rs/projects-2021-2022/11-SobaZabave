#include "../include/othellogame.h"

OthelloGame::OthelloGame(GameSettings &gs)
    : typeOfGame(gs.getgameType()), currentState(), AI(gs), gameSettings(gs) {}

OthelloGame::~OthelloGame() {
  delete player1;
  delete player2;
}

auto OthelloGame::getGameSettings() -> GameSettings { return gameSettings; }

auto OthelloGame::get_next_move() -> int {
  return AI.getNextAIMove(currentState);
}

auto OthelloGame::calculatePlayerFields() const -> QPair<int, int> {
  QPair<int, int> result{0, 0};
  for (int i = 0; i < currentState.TABLEDIMENSION; ++i)
    for (int j = 0; j < currentState.TABLEDIMENSION; ++j)
      if (currentState.getTableField(i, j) == player1->id())
        result.first++;
      else if (currentState.getTableField(i, j) == player2->id())
        result.second++;

  return result;
}

void OthelloGame::doMove(int move) {
  if (move == CANT_MOVE) {
    OthelloStateController::playNoMove(currentState);
    return;
  }
  OthelloStateController::playMove(currentState, move);
  QPair<int, int> result = calculatePlayerFields();
  player1->setNumOfFigures(result.first);
  player2->setNumOfFigures(result.second);
}

auto OthelloGame::getWinner() const -> int {
  int suma = 0;
  for (int i = 0; i < boardDimension; i++) {
    for (int j = 0; j < boardDimension; ++j) {
      suma += currentState.getTableField(i, j);
    }
  }
  return suma > 0 ? player1->id() : player2->id();
}

void OthelloGame::showMessageBox(QString content) const {
  QMessageBox mb;
  mb.setText(content);
  mb.setWindowTitle("Upozorenje");
  mb.exec();
}
