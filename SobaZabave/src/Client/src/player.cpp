#include "../include/player.h"

Player::Player(int id) : m_id(id) {}

auto Player::id() const -> int { return m_id; }

void Player::setId(int id) { m_id = id; }

auto Player::username() const -> QString { return m_username; }

void Player::setUsername(const QString &username) { m_username = username; }

auto Player::numOfFigures() const -> int { return m_numOfFigures; }

void Player::setNumOfFigures(int numOfFigures) {
  m_numOfFigures = numOfFigures;
}
