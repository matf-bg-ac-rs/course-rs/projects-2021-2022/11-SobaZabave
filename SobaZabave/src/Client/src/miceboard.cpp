#include "../include/miceboard.h"

using namespace std;

MiceBoard::MiceBoard() : m_table(QVector<int>(numberOfFields, emptyField)) {
  initializePositions();
  initializeGraph();
}

MiceBoard::~MiceBoard() = default;

auto MiceBoard::getTable() const -> const QVector<int> & { return m_table; }

auto MiceBoard::getGraph() const -> const QVector<QPair<int, int>> & {
  return m_graph;
}

auto MiceBoard::getEmptyPositions() -> unordered_set<int> & {
  return m_emptyPositions;
}

void MiceBoard::setTable(const int destination, const int value) {
  m_table[destination] = value;
}

auto MiceBoard::getValidMovementsPhase0and2() const
    -> const unordered_set<int> & {
  return m_emptyPositions;
}

auto MiceBoard::getValidMovementsPhase1(const int player) const
    -> const QVector<QPair<int, int>> {
  QVector<QPair<int, int>> valid;
  int graphSize = m_graph.size();
  for (int i = 0; i < graphSize; i++) {
    if (m_table[m_graph[i].first] == player &&
        m_table[m_graph[i].second] == 0) {
      valid.push_back(qMakePair(m_graph[i].first, m_graph[i].second));
    } else if (m_table[m_graph[i].first] == 0 &&
               m_table[m_graph[i].second] == player) {
      valid.push_back(qMakePair(m_graph[i].second, m_graph[i].first));
    }
  }

  return valid;
}

auto MiceBoard::checkIsValidMovePhase0and2(const int move) const -> bool {
  const unordered_set<int> validMoves = getValidMovementsPhase0and2();
  if (validMoves.find(move) != validMoves.end()) {
    return true;
  }
  return false;
}

auto MiceBoard::checkIsValidMovePhase1(const QPair<int, int> &move,
                                       const int player) const -> bool {
  const QVector<QPair<int, int>> validMoves = getValidMovementsPhase1(player);
  int validMovesSize = validMoves.size();
  for (int i = 0; i < validMovesSize; i++) {
    if (move == validMoves[i]) {
      return true;
    }
  }
  return false;
}

auto MiceBoard::getNumberOfFields() const -> int { return numberOfFields; }

void MiceBoard::initializePositions() {
  for (int i = 0; i < numberOfFields; i++) {
    m_emptyPositions.insert(i);
  }
}

void MiceBoard::initializeGraph() {
  m_graph.push_back(qMakePair(0, 1));
  m_graph.push_back(qMakePair(1, 2));
  m_graph.push_back(qMakePair(2, 3));
  m_graph.push_back(qMakePair(3, 4));
  m_graph.push_back(qMakePair(4, 5));
  m_graph.push_back(qMakePair(5, 6));
  m_graph.push_back(qMakePair(6, 7));
  m_graph.push_back(qMakePair(7, 0));

  m_graph.push_back(qMakePair(1, 9));
  m_graph.push_back(qMakePair(3, 11));
  m_graph.push_back(qMakePair(5, 13));
  m_graph.push_back(qMakePair(7, 15));

  m_graph.push_back(qMakePair(8, 9));
  m_graph.push_back(qMakePair(9, 10));
  m_graph.push_back(qMakePair(10, 11));
  m_graph.push_back(qMakePair(11, 12));
  m_graph.push_back(qMakePair(12, 13));
  m_graph.push_back(qMakePair(13, 14));
  m_graph.push_back(qMakePair(14, 15));
  m_graph.push_back(qMakePair(15, 8));

  m_graph.push_back(qMakePair(9, 17));
  m_graph.push_back(qMakePair(11, 19));
  m_graph.push_back(qMakePair(13, 21));
  m_graph.push_back(qMakePair(15, 23));

  m_graph.push_back(qMakePair(16, 17));
  m_graph.push_back(qMakePair(17, 18));
  m_graph.push_back(qMakePair(18, 19));
  m_graph.push_back(qMakePair(19, 20));
  m_graph.push_back(qMakePair(20, 21));
  m_graph.push_back(qMakePair(21, 22));
  m_graph.push_back(qMakePair(22, 23));
  m_graph.push_back(qMakePair(23, 16));
}
