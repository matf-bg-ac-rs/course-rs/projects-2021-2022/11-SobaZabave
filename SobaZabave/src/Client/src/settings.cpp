#include "include/settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) : QWidget(parent), ui(new Ui::Settings) {
  ui->setupUi(this);
}

Settings::~Settings() { delete ui; }

void Settings::setUsername(QString username) {
  ui->usernameLabelSettings->setText(username);
}

void Settings::on_backButtonSettings_clicked() { emit BackClicked(); }

void Settings::on_difficultyButton_clicked() {

  if (difficultyLevel == 1)
    gameDifficuly = GameSettings::GameDifficulty::EASY;
  else if (difficultyLevel == 2)
    gameDifficuly = GameSettings::GameDifficulty::MEDIUM;
  else if (difficultyLevel == 3)
    gameDifficuly = GameSettings::GameDifficulty::HARD;
  else
    gameDifficuly = GameSettings::GameDifficulty::VERYHARD;

  emit DifficultySetClicked(gameDifficuly);
  emit BackClicked();
}

void Settings::on_spinBox_valueChanged(const QString &arg1) {
  difficultyLevel = arg1.toInt();
}
