#include "include/mainwindow.h"
#include "include/micegamewindowlocal.h"
#include "include/othellogamewindow.h"
#include "include/settings.h"
#include <QDebug>
#include <QFile>
#include <QFont>
#include <QFontDatabase>
#include <QIODevice>
#include <QTextStream>
#include <QtWidgets/QApplication>
#include <iostream>

#include <QApplication>

auto main(int argc, char *argv[]) -> int {
  QApplication a(argc, argv);

  QFile styleSheetFile(":/resources/resources/style.qss");
  styleSheetFile.open(QFile::ReadOnly);
  QString styleSheet = QLatin1String(styleSheetFile.readAll());
  a.setStyleSheet(styleSheet);

  int id = QFontDatabase::addApplicationFont(
      ":/resources/resources/Astrobia-K47W.ttf");
  QString family = QFontDatabase::applicationFontFamilies(id).at(0);
  QFont monospace(family);
  a.setFont(monospace);

  MainWindow w;
  w.show();

  return a.exec();
}
