#include "../include/othellostate.h"

Othellostate::Othellostate() {
  table[3][3] = player1;
  table[4][4] = player1;
  table[3][4] = player2;
  table[4][3] = player2;

  onMove = player1;
}

Othellostate::Othellostate(const Othellostate &os) {
  onMove = os.getOnMove();
  for (int i = 0; i < TABLEDIMENSION; ++i) {
    for (int j = 0; j < TABLEDIMENSION; ++j) {
      table[i][j] = os.getTableField(i, j);
    }
  }
}

Othellostate::~Othellostate() = default;

auto Othellostate::isEnd() const -> bool {
  for (int i = 0; i < TABLEDIMENSION; ++i) {
    for (int j = 0; j < TABLEDIMENSION; ++j) {
      if (table[i][j] == 0)
        return false;
    }
  }
  return true;
}

auto Othellostate::getRowAndCollumnFromMove(int move) -> QPair<int, int> {
  QPair<int, int> rowColumn;
  rowColumn.first = move / 10;
  rowColumn.second = move % 10;
  return rowColumn;
}

void Othellostate::putOnTable(int move) {
  QPair<int, int> rowColumn;
  rowColumn = getRowAndCollumnFromMove(move);
  table[rowColumn.first][rowColumn.second] = getOnMove();
}

auto Othellostate::getTableField(int row, int column) const -> int {
  return table[row][column];
}

auto Othellostate::getOnMove() const -> int { return onMove; }

void Othellostate::changeOnMove() { onMove *= -1; }
