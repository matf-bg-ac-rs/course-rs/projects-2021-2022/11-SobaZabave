#include "../include/boardscene.h"

BoardScene::BoardScene(QObject *parent) : QGraphicsScene(parent) {}

BoardScene::~BoardScene() {
  for (auto field : m_fields) {
    delete field;
  }
  m_fields.clear();

  for (auto line : m_lines) {
    delete line;
  }
  m_lines.clear();
}

auto BoardScene::fieldPosition(const int index) const -> QPointF {
  return m_fields[index]->position();
}

void BoardScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) {
  QGraphicsScene::mousePressEvent(mouseEvent);

  if (mouseEvent->isAccepted()) {
    return;
  }
  emit signalClickedSomething(mouseEvent->scenePos());
}
