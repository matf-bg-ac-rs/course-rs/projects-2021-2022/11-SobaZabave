#include "../include/client.h"

Client::Client(QObject *parent) : QObject(parent) {
  socket = new QTcpSocket(this);
}
Client::~Client() { delete socket; }

auto Client::doConnect() -> bool {

  connect(socket, &QTcpSocket::connected, this, &Client::connected);
  connect(socket, &QTcpSocket::disconnected, this, &Client::disconnected);
  connect(socket, &QTcpSocket::bytesWritten, this, &Client::bytesWritten);
  connect(socket, &QTcpSocket::readyRead, this, &Client::readyRead);

  qDebug() << "connecting...";

  socket->connectToHost(QHostAddress::LocalHost, 12345);

  if (!socket->waitForConnected(5000)) {
    qDebug() << "Error: " << socket->errorString();
  }

  return socket->waitForConnected();
}

void Client::connected() { qDebug() << "connected..."; }

void Client::disconnected() { qDebug() << "disconnected..."; }

void Client::bytesWritten(qint64 bytes) {
  qDebug() << bytes << " bytes written...";
}

void Client::readyRead() {

  QString message = socket->readAll();
  if (message == "You are client1" || message == "You are client2") {
    if (message == "You are client1") {
      client1_ind = true;
      yourTurn = true;
    }

    else {
      client1_ind = false;
      yourTurn = false;
    }
  } else {
    int index = message.toInt();
    emit sendIndexToWindow(index);
  }
}
void Client::readIndexFromWindow(int index) {
  writeData(QByteArray::number(index));
}

auto Client::writeData(QByteArray data) -> bool {
  qDebug() << socket->state();
  if (socket->state() == QAbstractSocket::ConnectedState) {
    socket->write(data);
    return socket->waitForBytesWritten();
  } else
    return false;
}
