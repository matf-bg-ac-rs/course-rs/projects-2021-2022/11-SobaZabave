#include "include/mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_othellogamewindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent), ui(new Ui::MainWindow), gameSettings() {
  ui->setupUi(this);
  ui->stackedWidget->setCurrentIndex(0);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::moveHomeFromSettings() {
  if (_settings != nullptr) {
    _settings->close();
    delete _settings;
    _settings = nullptr;
  }
  this->show();
}

void MainWindow::moveHomeFromOthello() {
  if (_othelloGameWindow != nullptr) {
    _othelloGameWindow->close();
    delete _othelloGameWindow;
    _othelloGameWindow = nullptr;
  }
  this->show();
}

void MainWindow::moveHomeFromMiceLocal() {
  if (_miceGameWindow != nullptr) {
    _miceGameWindow->close();
    delete _miceGameWindow;
    _miceGameWindow = nullptr;
  }
  this->show();
}

void MainWindow::moveHomeFromMiceMultiplayer() {
  if (_miceGameWindow != nullptr) {
    _miceGameWindow->close();
    delete _miceGameWindow;
    _miceGameWindow = nullptr;
  }
  this->show();
}

void MainWindow::on_logInButton_clicked() {
  username = ui->usernameEdit->text();
  username = username.trimmed();

  if (username == "") {
    showMessageBox("Morate uneti username   ");
    return;
  }

  ui->userLabelChooseGame->setText(username);
  ui->userLabelMG->setText(username);
  ui->userLabelOG->setText(username);
  ui->usernameEdit->setText("");
  ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_settingsButton_clicked() {
  _settings = new Settings();
  _settings->setUsername(username);
  connect(_settings, &Settings::BackClicked, this,
          &MainWindow::moveHomeFromSettings);
  connect(_settings, &Settings::DifficultySetClicked, this,
          &MainWindow::on_setDifficulty);
  _settings->show();
  this->hide();
}

void MainWindow::on_setDifficulty(GameSettings::GameDifficulty gameDifficulty) {
  gameSettings.setDifficultyLevel(gameDifficulty);
  showMessageBox("Uspesno je podesena tezina igre");
  return;
}

void MainWindow::on_playButtonOthello_clicked() {

  if (localOthelloGame) {
    gameSettings.setGameType(GameSettings::GameType::LOCAL);
  } else if (singlePlayerOthelloGame) {
    gameSettings.setGameType(GameSettings::GameType::SINGLEPLAYER);
  } else {
    showMessageBox("Morate izabrati nacin igre!    ");
    return;
  }

  _othelloGameWindow = new OthelloGameWindow(nullptr, gameSettings);
  _othelloGameWindow->setUsername(username);
  connect(_othelloGameWindow, &OthelloGameWindow::BackClicked, this,
          &MainWindow::moveHomeFromOthello);
  _othelloGameWindow->show();
  this->hide();
}

void MainWindow::on_playButtonMice_clicked() {
  if (localPlayerMiceGame) {
    _miceGameWindow = new MiceGameWindowLocal();
    _miceGameWindow->setUsername(username);
    connect(_miceGameWindow, &MiceGameWindow::BackClicked, this,
            &MainWindow::moveHomeFromMiceLocal);
    _miceGameWindow->show();
    this->hide();
    return;
  } else if (multiPlayerMiceGame) {
    _miceGameWindow = new MiceGameWindowServer();
    _miceGameWindow->setUsername(username);
    connect(_miceGameWindow, &MiceGameWindow::BackClicked, this,
            &MainWindow::moveHomeFromMiceMultiplayer);
    _miceGameWindow->show();
    this->hide();
  } else {
    showMessageBox("Morate izabrati nacin igre!    ");
    return;
  }
}

void MainWindow::on_backButtonInstructions_clicked() {
  ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_radioButtonLocalOthelloGame_clicked() {
  localOthelloGame = true;
  singlePlayerOthelloGame = false;
}

void MainWindow::on_radioButtonSingleOthelloGame_clicked() {
  localOthelloGame = false;
  singlePlayerOthelloGame = true;
}

void MainWindow::on_radioButtonMPMiceGame_clicked() {
  multiPlayerMiceGame = true;
  localPlayerMiceGame = false;
}

void MainWindow::on_radioButtonLocalMiceGame_clicked() {
  multiPlayerMiceGame = false;
  localPlayerMiceGame = true;
}

void MainWindow::on_instructionsButton_clicked() {
  ui->stackedWidget->setCurrentIndex(4);
}

void MainWindow::on_backButtonCG_clicked() {
  ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_othelloButtonCG_clicked() {
  ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::on_miceButtonCG_clicked() {
  ui->stackedWidget->setCurrentIndex(3);
}

void MainWindow::on_backButtonOG_clicked() {
  ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_backButtonMice_clicked() {
  ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::showMessageBox(QString content) const {
  QMessageBox mb;
  mb.setText(content);
  mb.setWindowTitle("Upozorenje");
  mb.exec();
}
