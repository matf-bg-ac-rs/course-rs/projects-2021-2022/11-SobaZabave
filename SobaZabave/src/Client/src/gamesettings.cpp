#include "../include/gamesettings.h"

GameSettings::GameSettings()
    : difficultyLevel(GameSettings::GameDifficulty::EASY),
      gameType(GameSettings::GameType::LOCAL) {}
GameSettings::GameSettings(GameSettings &gs)
    : difficultyLevel(gs.getdifficultyLevel()), gameType(gs.getgameType()) {}

void GameSettings::setDifficultyLevel(
    const GameSettings::GameDifficulty difficultyLevel) {
  this->difficultyLevel = difficultyLevel;
}

void GameSettings::setGameType(const GameSettings::GameType gameType) {
  this->gameType = gameType;
}

auto GameSettings::getgameType() -> GameSettings::GameType { return gameType; }

auto GameSettings::getdifficultyLevel() -> GameSettings::GameDifficulty {
  return difficultyLevel;
}
