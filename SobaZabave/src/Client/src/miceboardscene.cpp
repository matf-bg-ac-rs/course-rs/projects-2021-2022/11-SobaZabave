#include "../include/miceboardscene.h"
#include <QGraphicsSceneMouseEvent>

MiceBoardScene::MiceBoardScene(QObject *parent) : BoardScene(parent) {}

void MiceBoardScene::addAllFields(qreal viewWidth, qreal viewHeight,
                                  qreal offset) {
  m_fields.push_back(new MiceField(QPointF(0, 0)));
  m_fields.push_back(new MiceField(QPointF(viewWidth / 2 - offset, 0)));
  m_fields.push_back(new MiceField(QPointF(viewWidth - 2 * offset, 0)));

  m_fields.push_back(
      new MiceField(QPointF(viewWidth - 2 * offset, viewHeight / 2 - offset)));
  m_fields.push_back(
      new MiceField(QPointF(viewWidth - 2 * offset, viewHeight - 2 * offset)));

  m_fields.push_back(
      new MiceField(QPointF(viewWidth / 2 - offset, viewHeight - 2 * offset)));
  m_fields.push_back(new MiceField(QPointF(0, viewHeight - 2 * offset)));

  m_fields.push_back(new MiceField(QPointF(0, viewHeight / 2 - offset)));

  m_fields.push_back(
      new MiceField(QPointF(viewWidth / 6 - offset, viewHeight / 6 - offset)));
  m_fields.push_back(
      new MiceField(QPointF(viewWidth / 2 - offset, viewHeight / 6 - offset)));
  m_fields.push_back(new MiceField(
      QPointF(5 * viewWidth / 6 - offset, viewHeight / 6 - offset)));

  m_fields.push_back(new MiceField(
      QPointF(5 * viewWidth / 6 - offset, viewHeight / 2 - offset)));
  m_fields.push_back(new MiceField(
      QPointF(5 * viewWidth / 6 - offset, 5 * viewWidth / 6 - offset)));

  m_fields.push_back(new MiceField(
      QPointF(viewWidth / 2 - offset, 5 * viewWidth / 6 - offset)));
  m_fields.push_back(new MiceField(
      QPointF(viewWidth / 6 - offset, 5 * viewWidth / 6 - offset)));
  m_fields.push_back(
      new MiceField(QPointF(viewWidth / 6 - offset, viewHeight / 2 - offset)));

  m_fields.push_back(new MiceField(
      QPointF(2 * viewWidth / 6 - offset, 2 * viewHeight / 6 - offset)));
  m_fields.push_back(new MiceField(
      QPointF(viewWidth / 2 - offset, 2 * viewHeight / 6 - offset)));
  m_fields.push_back(new MiceField(
      QPointF(4 * viewWidth / 6 - offset, 2 * viewHeight / 6 - offset)));

  m_fields.push_back(new MiceField(
      QPointF(4 * viewWidth / 6 - offset, viewHeight / 2 - offset)));
  m_fields.push_back(new MiceField(
      QPointF(4 * viewWidth / 6 - offset, 4 * viewWidth / 6 - offset)));

  m_fields.push_back(new MiceField(
      QPointF(viewWidth / 2 - offset, 4 * viewWidth / 6 - offset)));
  m_fields.push_back(new MiceField(
      QPointF(2 * viewWidth / 6 - offset, 4 * viewWidth / 6 - offset)));
  m_fields.push_back(new MiceField(
      QPointF(2 * viewWidth / 6 - offset, viewHeight / 2 - offset)));

  addAllLines(offset, viewWidth, viewHeight);

  for (int i = 0; i < numOfFields; ++i) {
    const auto field = static_cast<MiceField *>(m_fields[i]);
    addItem(field);
  }
}

void MiceBoardScene::addAllLines(qreal offset, qreal viewWidth,
                                 qreal viewHeight) {

  for (int i = 0; i < 8; ++i) {
    const auto line = new QGraphicsLineItem(
        (fieldPosition(i)).x() + offset, (fieldPosition(i)).y() + offset,
        fieldPosition((i + 1) % 8).x() + offset,
        fieldPosition((i + 1) % 8).y() + offset);
    m_lines.push_back(line);
    addItem(line);
  }

  for (int i = 8; i < 16; ++i) {
    const auto line = new QGraphicsLineItem(
        fieldPosition(i).x() + offset, fieldPosition(i).y() + offset,
        fieldPosition((i + 1) % 8 + 8).x() + offset,
        fieldPosition((i + 1) % 8 + 8).y() + offset);
    m_lines.push_back(line);
    addItem(line);
  }

  for (int i = 16; i < 23; ++i) {
    const auto line = new QGraphicsLineItem(
        fieldPosition(i).x() + offset, fieldPosition(i).y() + offset,
        fieldPosition(i + 1).x() + offset, fieldPosition(i + 1).y() + offset);
    m_lines.push_back(line);
    addItem(line);
  }

  const auto line = new QGraphicsLineItem(
      fieldPosition(23).x() + offset, fieldPosition(23).y() + offset,
      fieldPosition(16).x() + offset, fieldPosition(16).y() + offset);
  m_lines.push_back(line);
  addItem(line);

  for (int i = 1; i < 8; i += 2) {
    const auto line1 = new QGraphicsLineItem(
        fieldPosition(i).x() + offset, fieldPosition(i).y() + offset,
        fieldPosition(i + 8).x() + offset, fieldPosition(i + 8).y() + offset);
    m_lines.push_back(line1);
    addItem(line1);

    const auto line2 = new QGraphicsLineItem(
        fieldPosition(i + 8).x() + offset, fieldPosition(i + 8).y() + offset,
        fieldPosition(i + 16).x() + offset, fieldPosition(i + 16).y() + offset);
    m_lines.push_back(line2);
    addItem(line2);
  }
}
