#include "include/micegamewindowlocal.h"

MiceGameWindowLocal::MiceGameWindowLocal(QWidget *parent)
    : MiceGameWindow(parent) {
  connect(m_boardScene, &BoardScene::signalClickedSomething, this,
          &MiceGameWindowLocal::onFieldClick);
}

MiceGameWindowLocal::~MiceGameWindowLocal() {
  delete ui;
  delete m_boardScene;
  if (m_miceGame != nullptr) {
    delete m_miceGame;
  }
}

void MiceGameWindowLocal::onFieldClick(QPointF pos) {
  auto item = m_boardScene->itemAt(pos, QTransform());
  if (item == nullptr) {
    return;
  }

  auto fieldItem = static_cast<MiceField *>(item);
  int index = getFieldIndexByPosition(fieldItem);

  bool returnValue = eatAndUpdateBoard(fieldItem, index);
  if (returnValue == true) {
    return;
  }

  returnValue = moveAndUpdateBoard(index);
  if (returnValue == false) {
    return;
  }

  ui->gvBoard->viewport()->update();

  if (m_miceGame->checkIsOver()) {
    m_miceGame->showMessageBox("Igra je zavrsena!");
    return;
  }
}
