#include "../include/micegame.h"

using namespace std;

MiceGame::MiceGame()
    : m_player1(new MicePlayer(1)), m_player2(new MicePlayer(2)),
      m_currentPlayer(m_player1) {}

MiceGame::~MiceGame() {
  delete m_player1;
  delete m_player2;
}

auto MiceGame::tryMove(const int position) -> bool {
  if (currentPlayerPhase() == MicePlayer::Phase::Phase0) {
    if (board().checkIsValidMovePhase0and2(position)) {
      setDestination(position);
      playMove();
      return true;
    } else {
      // showMessageBox("Zauzeto polje!");
      return false;
    }
  }

  if (m_moveIndicator == false) {
    if (currentPlayerId() != board().getTable()[position]) {
      // showMessageBox("Ovo nije Vasa figura!");
      return false;
    } else {
      setSource(position);
      setMoveIndicator(true);
      return true;
    }
  }

  return tryMovePhase1or2(position);
}

auto MiceGame::tryEat(const int position) -> bool {
  MicePlayer *opponent = otherPlayer();
  if (board().getTable()[position] != opponent->id()) {
    return false;
  }
  if (!eatIfYouCan(position)) {
    return false;
  }
  tryToChangePhase();
  return true;
}

void MiceGame::playMovePhase0() {
  m_board.setTable(m_destination, m_currentPlayer->id());
  m_board.getEmptyPositions().erase(m_destination);
}

void MiceGame::playMovePhase1And2() {
  m_board.setTable(m_destination, m_currentPlayer->id());
  m_board.setTable(m_source, 0);
  m_board.getEmptyPositions().insert(m_source);
  m_board.getEmptyPositions().erase(m_destination);
}

void MiceGame::playMove() {
  if (currentPlayerPhase() == MicePlayer::Phase::Phase0) {
    playMovePhase0();
  } else {
    playMovePhase1And2();
  }
  m_numOfMoves++;

  if (formsMill(m_destination)) {
    setEatIndicator(true);
  }

  if (!m_eatIndicator)
    m_currentPlayer = otherPlayer();

  tryToChangePhase();
}

void MiceGame::tryToChangePhase() {
  if (m_numOfMoves == numOfPlayers * numOfPieces) {
    m_player1->setPhase(MicePlayer::Phase::Phase1);
    m_player2->setPhase(MicePlayer::Phase::Phase1);
  }

  if (m_player2->numOfFigures() == 3) {
    m_player1->setPhase(MicePlayer::Phase::Phase2);
  }

  if (m_player2->numOfFigures() == 3) {
    m_player2->setPhase(MicePlayer::Phase::Phase2);
  }
}

auto MiceGame::currentPlayer() const -> MicePlayer * { return m_currentPlayer; }

auto MiceGame::tryMovePhase1or2(int position) -> bool {
  bool moveIsValid = false;
  if (currentPlayerPhase() == MicePlayer::Phase::Phase1) {
    moveIsValid = board().checkIsValidMovePhase1(qMakePair(m_source, position),
                                                 m_currentPlayer->id());
  } else {
    moveIsValid = board().checkIsValidMovePhase0and2(position);
  }

  if (moveIsValid) {
    setDestination(position);
    setMoveIndicator(false);
    playMove();
    return true;
  } else {
    // showMessageBox("Ne mozete ovde postaviti figuru!");
    setMoveIndicator(false);
    return false;
  }
}

auto MiceGame::otherPlayer() const -> MicePlayer * {
  return m_currentPlayer == m_player1 ? m_player2 : m_player1;
}

auto MiceGame::currentPlayerPhase() const -> MicePlayer::Phase {
  return m_currentPlayer->phase();
}

auto MiceGame::currentPlayerId() const -> int { return m_currentPlayer->id(); }

void MiceGame::setEatIndicator(bool eatIndicator) {
  m_eatIndicator = eatIndicator;
}

void MiceGame::setCurrentPlayer(MicePlayer *currentPlayer) {
  m_currentPlayer = currentPlayer;
}

auto MiceGame::player2() const -> MicePlayer * { return m_player2; }

void MiceGame::setPlayer2(MicePlayer *player2) { m_player2 = player2; }

auto MiceGame::player1() const -> MicePlayer * { return m_player1; }

void MiceGame::setPlayer1(MicePlayer *player1) { m_player1 = player1; }

auto MiceGame::source() const -> int { return m_source; }

auto MiceGame::destination() const -> int { return m_destination; }

auto MiceGame::getWinner() const -> MiceGame::GameResult {
  if (wonPlayer2()) {
    return GameResult::WinPlayer2;
  }

  if (wonPlayer1()) {
    return GameResult::WinPlayer1;
  }

  if (isDraw()) {
    return GameResult::Draw;
  }

  return GameResult::NotOver;
}

auto MiceGame::wonPlayer1() const -> bool {
  return m_player2->numOfFigures() == 2;
}

auto MiceGame::wonPlayer2() const -> bool {
  return m_player1->numOfFigures() == 2;
}

auto MiceGame::isDraw() const -> bool {
  return (m_player1->numOfFigures() == 3 && m_player2->numOfFigures() == 3);
}

auto MiceGame::eatIfYouCan(const int position) -> bool {
  MicePlayer *opponent = otherPlayer();
  if (!canEatAtThisPos(position)) {
    return false;
  }

  if (m_board.getTable()[position] == opponent->id()) {
    m_board.setTable(position, 0);
    opponent->setNumOfFigures(opponent->numOfFigures() - 1);
    m_board.getEmptyPositions().insert(position);
  }

  m_eatIndicator = false;
  m_currentPlayer = otherPlayer();
  return true;
}

auto MiceGame::board() const -> const MiceBoard & { return m_board; }

auto MiceGame::eatIndicator() const -> bool { return m_eatIndicator; }

auto MiceGame::checkIsOver() const -> bool {
  if (getWinner() != GameResult::NotOver) {
    return true;
  }
  return false;
}

void MiceGame::setMoveIndicator(bool value) { m_moveIndicator = value; }

void MiceGame::setSource(int value)

{
  m_source = value;
}
void MiceGame::setDestination(int value)

{
  m_destination = value;
}

auto MiceGame::canEatAtThisPos(const int position) const -> bool {
  if (formsMill(position)) {
    if (count(m_board.getTable().begin(), m_board.getTable().end(),
              m_board.getTable()[position]) == 3)
      return true;
    else
      return false;
  }

  return true;
}

auto MiceGame::formsMill(const int position) const -> bool {
  int square = getPositionSquare(position);

  if (position % 2 == 0) {
    if (formsMillOnEvenPosition(position - 8 * square, square)) {
      return true;
    }
  }

  else {
    if (formsMillOnOddPosition(position - 8 * square, square)) {
      return true;
    }
  }

  return false;
}

auto MiceGame::getPositionSquare(const int position) const -> int {
  if (position >= 0 && position <= 7) {
    return 0;
  }
  if (position >= 8 && position <= 15) {
    return 1;
  }
  if (position >= 16 && position <= 23) {
    return 2;
  }
  return -1;
}

auto MiceGame::formsMillOnEvenPosition(const int position,
                                       const int square) const -> bool {
  int firstPositionToCheck =
      m_board.getTable()[(position - 1 + 8) % 8 + square * 8];
  int secondPositionToCheck =
      m_board.getTable()[(position - 2 + 8) % 8 + square * 8];
  bool firstCondition =
      m_board.getTable()[position + square * 8] == firstPositionToCheck &&
      m_board.getTable()[position + square * 8] == secondPositionToCheck;

  firstPositionToCheck = m_board.getTable()[(position + 1) % 8 + square * 8];
  secondPositionToCheck = m_board.getTable()[(position + 2) % 8 + square * 8];
  bool secondCondition =
      m_board.getTable()[position + square * 8] == firstPositionToCheck &&
      m_board.getTable()[position + square * 8] == secondPositionToCheck;

  return firstCondition || secondCondition;
}

auto MiceGame::formsMillOnOddPosition(const int position,
                                      const int square) const -> bool {
  int firstPositionToCheck = m_board.getTable()[position + 8];
  int secondPositionToCheck = m_board.getTable()[position + 16];
  bool firstCondition = m_board.getTable()[position] == firstPositionToCheck &&
                        m_board.getTable()[position] == secondPositionToCheck;

  firstPositionToCheck = m_board.getTable()[(position - 1) % 8 + square * 8];
  secondPositionToCheck = m_board.getTable()[(position + 1) % 8 + square * 8];
  bool secondCondition =
      m_board.getTable()[position + square * 8] == firstPositionToCheck &&
      m_board.getTable()[position + square * 8] == secondPositionToCheck;

  return firstCondition || secondCondition;
}

void MiceGame::showMessageBox(QString content) const {
  QMessageBox mb;
  mb.setText(content);
  mb.setWindowTitle("Upozorenje");
  mb.exec();
}
