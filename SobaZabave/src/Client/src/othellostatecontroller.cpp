#include "../include/othellostatecontroller.h"

static auto isOutOfBound(int bound, int row, int col) -> bool {
  return (row > bound || row < 0 || col > bound || col < 0);
}

static auto possibleShifts() -> QVector<QPair<int, int>> {
  QVector<QPair<int, int>> shifts;
  const int RIGHTDIRECTION = 1;
  const int LEFTDIRECTION = -1;
  const int UPDIRECTION = -1;
  const int DOWNDIRECTION = 1;
  shifts = {{UPDIRECTION, LEFTDIRECTION},
            {UPDIRECTION, 0},
            {UPDIRECTION, RIGHTDIRECTION},
            {0, LEFTDIRECTION},
            {0, RIGHTDIRECTION},
            {DOWNDIRECTION, LEFTDIRECTION},
            {DOWNDIRECTION, 0},
            {DOWNDIRECTION, RIGHTDIRECTION}};
  return shifts;
}

static void flipTable(Othellostate &os, int row, int col) {
  QVector<QVector<int>> collectionForFliping;
  int otherPlayer = os.getTableField(row, col) * -1;

  QVector<QPair<int, int>> possibleshifts = possibleShifts();

  for (auto shift : possibleshifts) {

    int currentRow = row + shift.first;
    int currCol = col + shift.second;

    if (isOutOfBound(os.TABLEDIMENSION, currentRow, currCol))
      continue;

    int colorPos = os.getTableField(currentRow, currCol);
    bool flipDirection = false;

    if (colorPos == otherPlayer) {
      while (colorPos == otherPlayer) {
        currentRow += shift.first;
        currCol += shift.second;

        if (isOutOfBound(os.TABLEDIMENSION, currentRow, currCol))
          break;

        colorPos = os.getTableField(currentRow, currCol);
      }
      if (colorPos == otherPlayer * (-1))
        flipDirection = true;

      if (flipDirection) {
        currentRow = row + shift.first;
        currCol = col + shift.second;
        colorPos = os.getTableField(currentRow, currCol);
        while (colorPos == otherPlayer) {
          QVector<int> forFliping = {currentRow, currCol};
          collectionForFliping.push_back(forFliping);
          currentRow += shift.first;
          currCol += shift.second;
          colorPos = os.getTableField(currentRow, currCol);
        }
      }
    }
  }
  for (auto pos : collectionForFliping)
    os.putOnTable(pos[0] * 10 + pos[1]);
}

void OthelloStateController::playNoMove(Othellostate &os) { os.changeOnMove(); }

auto OthelloStateController::hasNextMove(const Othellostate &os) -> bool {
  return OthelloStateController::getNextValidMoves(os).length() > 0;
}

auto OthelloStateController::getRowAndCollumnFromMove(int move)
    -> QPair<int, int> {
  QPair<int, int> rowColumn;
  rowColumn.first = move / 10;
  rowColumn.second = move % 10;
  return rowColumn;
}

void OthelloStateController::playMove(Othellostate &os, int move) {
  os.putOnTable(move);
  QPair<int, int> rowCollumn = getRowAndCollumnFromMove(move);
  int row = rowCollumn.first;
  int col = rowCollumn.second;
  flipTable(os, row, col);
  os.changeOnMove();
}

auto OthelloStateController::getNextValidMoves(const Othellostate &os)
    -> QVector<int> {
  QVector<int> validMoves;

  for (int i = 0; i < os.TABLEDIMENSION; ++i)
    for (int j = 0; j < os.TABLEDIMENSION; ++j)
      if (os.getTableField(i, j) == 0 && isPossibleMove(os, i, j))
        validMoves.push_back(i * 10 + j);

  return validMoves;
}

auto OthelloStateController::isPossibleMove(const Othellostate &os, int row,
                                            int col) -> bool {

  int otherPlayer = (os.getOnMove() == 1) ? -1 : 1;
  QVector<QPair<int, int>> possibleshifts = possibleShifts();

  for (auto shift : possibleshifts) {

    if (isOutOfBound(os.TABLEDIMENSION - 1, row + shift.first,
                     col + shift.second))
      continue;

    int colorPos = os.getTableField(row + shift.first, col + shift.second);
    if (colorPos == otherPlayer) {
      int currentRow = row + shift.first;
      int currentCol = col + shift.second;

      while (colorPos == otherPlayer) {
        currentRow += shift.first;
        currentCol += shift.second;

        if (isOutOfBound(os.TABLEDIMENSION, currentRow, currentCol))
          break;

        colorPos = os.getTableField(currentRow, currentCol);
      }

      if (colorPos == os.getOnMove())
        return true;
    }
  }

  return false;
}
