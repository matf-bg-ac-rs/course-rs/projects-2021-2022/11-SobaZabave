#include "../include/othelloboardscene.h"
#include <QGraphicsSceneMouseEvent>

OthelloBoardScene::OthelloBoardScene(QObject *parent) : BoardScene(parent) {}

void OthelloBoardScene::addAllFields(qreal viewWidth, qreal viewHeight,
                                     qreal offset) {
  for (int i = 0; i < boardDimension; ++i) {
    for (int j = 0; j < boardDimension; ++j) {
      const auto field = new OthelloField(QPointF(
          i * offset + detailedPositioning, j * offset + detailedPositioning));

      // adding fields to diagonals
      if ((i == 3 && j == 3) || (i == 4 && j == 4)) {
        field->setCurrentFieldState(Field::FieldState::Player1);
      }
      if ((i == 3 && j == 4) || (i == 4 && j == 3)) {
        field->setCurrentFieldState(Field::FieldState::Player2);
      }

      m_fields.push_back(field);
    }
  }

  addAllLines(viewWidth, viewHeight, offset);

  for (int var = 0; var < numOfFields; var++) {
    const auto field = m_fields[var];
    addItem(field);
  }
}

void OthelloBoardScene::addAllLines(qreal viewWidth, qreal viewHeight,
                                    qreal offset) {
  for (int i = 0; i < boardDimension - 1; ++i) {
    const auto line = new QGraphicsLineItem(offset * (i + 1), 0,
                                            offset * (i + 1), viewHeight);
    m_lines.push_back(line);
    addItem(line);
  }

  for (int i = 0; i < boardDimension; ++i) {
    const auto line =
        new QGraphicsLineItem(0, offset * (i + 1), viewWidth, offset * (i + 1));
    m_lines.push_back(line);
    addItem(line);
  }
}
