#include "include/micegamewindowserver.h"
#include "ui_micegamewindow.h"

MiceGameWindowServer::MiceGameWindowServer(QWidget *parent)
    : MiceGameWindow(parent), m_tcpclient(new Client()) {
  m_tcpclient->doConnect();

  connect(this, &MiceGameWindowServer::sendIndexToClient, m_tcpclient,
          &Client::readIndexFromWindow);
  connect(m_tcpclient, &Client::sendIndexToWindow, this,
          &MiceGameWindowServer::readIndexFromClient);
  connect(m_boardScene, &BoardScene::signalClickedSomething, this,
          &MiceGameWindowServer::onFieldClick);
}

MiceGameWindowServer::~MiceGameWindowServer() {
  delete m_tcpclient;
  delete ui;
  delete m_boardScene;
  if (m_miceGame != nullptr) {
    delete m_miceGame;
  }
}

void MiceGameWindowServer::onFieldClick(QPointF pos) {
  if (!m_tcpclient->yourTurn) {
    m_miceGame->showMessageBox("Na redu je protivnik!");
    return;
  }
  auto item = m_boardScene->itemAt(pos, QTransform());
  if (item == nullptr) {
    return;
  }

  fieldItem = static_cast<MiceField *>(item);
  int index = getFieldIndexByPosition(fieldItem);
  emit sendIndexToClient(index);

  bool returnValue = eatAndUpdateBoard(fieldItem, index);
  if (returnValue == true) {
    return;
  }

  returnValue = moveAndUpdateBoard(index);
  if (returnValue == false) {
    return;
  }

  ui->gvBoard->viewport()->update();

  if (m_miceGame->checkIsOver()) {
    m_miceGame->showMessageBox("Igra je zavrsena");
    return;
  }
}

void MiceGameWindowServer::readIndexFromClient(int index) {
  m_tcpclient->yourTurn = true;
  bool returnValue = eatAndUpdateBoard(fieldItem, index);
  if (returnValue == true) {
    return;
  }

  returnValue = moveAndUpdateBoard(index);
  if (returnValue == false) {
    return;
  }
  ui->gvBoard->viewport()->update();
  if (m_miceGame->checkIsOver()) {
    m_miceGame->showMessageBox("Igra je zavrsena");
    return;
  }
}
