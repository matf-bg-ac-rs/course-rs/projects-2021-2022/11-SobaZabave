#include "../include/othellofield.h"

OthelloField::OthelloField(QPointF position) : Field(position) {}

void OthelloField::paint(QPainter *painter,
                         const QStyleOptionGraphicsItem *option,
                         QWidget *widget) {
  Q_UNUSED(option)
  Q_UNUSED(widget)

  if (m_currentFieldState == FieldState::Player1) {
    painter->drawImage(QRectF(0, 0, Size(), Size()),
                       QImage(QString(":/resources/resources/yellow.png")));
  } else if (m_currentFieldState == FieldState::Player2) {
    painter->drawImage(QRectF(0, 0, Size(), Size()),
                       QImage(QString(":/resources/resources/green2.png")));
  }
}
