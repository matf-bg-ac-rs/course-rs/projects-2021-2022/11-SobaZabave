#include "../include/miceplayer.h"

MicePlayer::MicePlayer(int id) : Player(id) { this->setNumOfFigures(9); }

auto MicePlayer::phase() const -> MicePlayer::Phase { return m_phase; }

void MicePlayer::setPhase(const Phase &phase) { m_phase = phase; }

auto MicePlayer::operator==(const MicePlayer &other) const -> bool {
  return id() == other.id();
}
