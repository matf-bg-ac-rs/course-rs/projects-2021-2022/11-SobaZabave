#include "../include/othellominmax.h"

OthelloMinMax::OthelloMinMax(GameSettings &gs) {
  gameDifficulty = gs.getdifficultyLevel();

  switch (gs.getdifficultyLevel()) {
  case GameSettings::GameDifficulty::EASY:
    difficulty = 2;
    break;
  case GameSettings::GameDifficulty::MEDIUM:
    difficulty = 3;
    break;
  case GameSettings::GameDifficulty::HARD:
    difficulty = 5;
    break;
  case GameSettings::GameDifficulty::VERYHARD:
    difficulty = 6;
    break;
  default:
    difficulty = 3;
  }
}

auto OthelloMinMax::getHeuristicScoreOnField(int row, int column) const -> int {
  return scoreBoard[row][column];
}

auto OthelloMinMax::getNextAIMove(const Othellostate &os) const -> int {
  if (!OthelloStateController::hasNextMove(os)) {
    return -1;
  }
  QPair<int, int> move = Min(os, difficulty, INT_MIN, INT_MAX);
  return move.first;
}

auto OthelloMinMax::Min(const Othellostate &os, int depth, int alpha,
                        int beta) const -> QPair<int, int> {
  if (depth == 0 || os.isEnd()) {
    return QPair(0, Evaluate(os));
  }

  int currentMin = INT_MAX;
  int currentBestNextMove = -1;

  for (int nextMove : OthelloStateController::getNextValidMoves(os)) {
    Othellostate ns(os);
    OthelloStateController::playMove(ns, nextMove);
    QPair<int, int> possible = Max(ns, depth - 1, alpha, beta);
    if (possible.second < currentMin) {
      currentMin = possible.second;
      currentBestNextMove = nextMove;
    }
    if (currentMin <= alpha)
      return QPair(currentBestNextMove, currentMin);

    if (currentMin < beta)
      beta = currentMin;
  }
  return QPair(currentBestNextMove, currentMin);
}

auto OthelloMinMax::Max(const Othellostate &os, int depth, int alpha,
                        int beta) const -> QPair<int, int> {
  if (depth == 0 or os.isEnd()) {
    return QPair(0, Evaluate(os));
  }

  int currentMax = INT_MIN;
  int currentBestNextMove = -1;

  for (int nextMove : OthelloStateController::getNextValidMoves(os)) {
    Othellostate ns(os);
    OthelloStateController::playMove(ns, nextMove);
    QPair<int, int> possible = Min(ns, depth - 1, alpha, beta);
    if (possible.second > currentMax) {
      currentMax = possible.second;
      currentBestNextMove = nextMove;
    }

    if (currentMax >= beta)
      return QPair(currentBestNextMove, currentMax);

    if (currentMax > alpha)
      alpha = currentMax;
  }
  return QPair(currentBestNextMove, currentMax);
}

auto OthelloMinMax::Evaluate(const Othellostate &os) const -> int {
  if (os.isEnd())
    return os.getOnMove() * INT_MAX;
  return HeuristicScore(os);
}

auto OthelloMinMax::HeuristicScore(const Othellostate &os) const -> int {
  int sum_score = 0;
  for (int i = 0; i < TABLEDIMENSION; ++i) {
    for (int j = 0; j < TABLEDIMENSION; ++j) {
      sum_score += os.getTableField(i, j) * getHeuristicScoreOnField(i, j);
    }
  }
  return sum_score;
}
