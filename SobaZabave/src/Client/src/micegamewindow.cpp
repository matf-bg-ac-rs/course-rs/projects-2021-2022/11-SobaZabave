#include "../include/micegamewindow.h"
#include "ui_micegamewindow.h"

MiceGameWindow::MiceGameWindow(QWidget *parent)
    : QWidget(parent), ui(new Ui::MiceGameWindow), m_miceGame(new MiceGame()),
      m_boardScene(new MiceBoardScene(this)) {

  setStyleSheet("background-image: url(:/resources/resources/back.jpg)");
  ui->setupUi(this);

  m_boardScene->setSceneRect(ui->gvBoard->rect());
  ui->gvBoard->setScene(m_boardScene);
  ui->gvBoard->setRenderHint(QPainter::Antialiasing);
  ui->gvBoard->setAlignment(Qt::AlignTop);

  ui->gvBoard->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  ui->gvBoard->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

  m_boardScene->addAllFields(ui->gvBoard->width(), ui->gvBoard->height(),
                             offset);
}

auto MiceGameWindow::miceGame() const -> MiceGame * { return m_miceGame; }

auto MiceGameWindow::getFieldIndexByPosition(MiceField *fieldItem) const
    -> int {
  int boardSize = m_miceGame->board().getNumberOfFields();
  for (int i = 0; i < boardSize; ++i) {
    if (m_boardScene->fieldPosition(i) == fieldItem->pos()) {
      return i;
    }
  }
  return -1;
}

auto MiceGameWindow::eatAndUpdateBoard(MiceField *fieldItem, const int index)
    -> bool {
  if (m_miceGame->eatIndicator()) {
    if (m_miceGame->tryEat(index)) {
      fieldItem->setCurrentFieldState(Field::FieldState::Empty);
      ui->gvBoard->viewport()->update();
      return true;
    } else {
      m_miceGame->showMessageBox("Ne mozete pojesti ovu figuru!");
      return true;
    }
  }
  return false;
}

auto MiceGameWindow::moveAndUpdateBoard(const int index) -> bool {
  if (!m_miceGame->tryMove(index)) {
    return false;
  }

  int boardSize = m_miceGame->board().getNumberOfFields();
  for (int i = 0; i < boardSize; ++i) {
    auto item =
        m_boardScene->itemAt(m_boardScene->fieldPosition(i), QTransform());
    auto fieldItem = static_cast<MiceField *>(item);
    fieldItem->setCurrentFieldState(
        static_cast<Field::FieldState>(m_miceGame->board().getTable()[i]));
  }
  return true;
}

MiceGameWindow::~MiceGameWindow() = default;

void MiceGameWindow::setUsername(QString username) {
  ui->usernameLabelMiceGame->setText(username);
}

void MiceGameWindow::on_btBack_clicked() { emit BackClicked(); }
