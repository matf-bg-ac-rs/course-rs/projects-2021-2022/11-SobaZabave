#  Project SobaZabave <img src="SobaZabave/src/Client/resources/yellow.png" width="30" hight="30"/><img src="SobaZabave/src/Client/resources/green2.png" width="32" hight="32"/>  <br><br>

## :memo: Opis igre
- Soba zabave je igrica koja korisniku omogucava da odabere jednu od ponudjene dve igre (Othello ili Mice). Obe igre se igraju na tabli sa zutim i zelenim diskovima. U zavisnosti od odabrane igre, igrac moze da izabere nacin igranja (local, singleplayer ili muliplayer). Postoje i podesavanja kojima se zadaje tezina igrice ako je odabran mod igre singleplayer. <br><br>

## :movie_camera: Demo snimak projekta 
- link: [Soba Zabave](https://youtu.be/zyOcyocTKTE) <br><br>

## Okruzenje
- [![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-olivia)](https://www.qt.io/download) <br><br>


## Programski jezik
- [![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-red)](https://www.cplusplus.com/)  *C++17*  <br>
- [![qt5](https://img.shields.io/badge/Framework-Qt5-blue)](https://doc.qt.io/qt-6/) *ili >* <br><br>


## :hammer: Instalacija :
- Preuzeti i instalirati [*Qt* i *Qt Creator*](https://www.qt.io/download).
- Ako je to potrebno,  nadograditi verziju C++ na C++17 <br><br>


## :wrench: Preuzimanje i pokretanje :
- 1. U terminalu se pozicionirati u zeljeni direktorijum
- 2. Klonirati repozitorijum komandom: `$ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/11-SobaZabave.git`
- 3. Otvoriti okruzenje *Qt Creator* i u njemu otvoriti Client.pro fajl
- 4. Pritisnuti dugme *Run* u donjem levom uglu ekrana <br>
    ### Za pokretanje servera :
- 1. Pored Client.pro fajla, neophodno je otvoriti i Server.pro fajl
- 2. Pokrenuti server desnim klikom na Server.pro fajl i klikom na dugme *Run*
- 3. Pokrenuti dva klijenta na isti nacin   <br><br>


## Developers

- [Andrijana Aleksic, 99/2018](https://gitlab.com/qndrijana)
- [Bogdan Markovic, 130/2018](https://gitlab.com/bogdanis)
- [Nikola Borjan, 10/2018](https://gitlab.com/Dzondzi)
- [Marko Bura, 141/2018](https://gitlab.com/markobura)
- [Svetlana Bicanin, 28/2018](https://gitlab.com/SvetlanaBicanin)
